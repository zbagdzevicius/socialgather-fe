export interface IEnvironment {
  production: boolean;
  backEndUrl: string;
  dev?: boolean;
}
