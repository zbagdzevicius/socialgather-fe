import { IEnvironment } from './environment.interface';

export const environment: IEnvironment = {
  production: true,
  backEndUrl: 'https://cronicler.eu',
};
