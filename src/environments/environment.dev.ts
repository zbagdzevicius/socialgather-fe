import { environment as prodEnvironment } from './environment.prod';
import { IEnvironment } from './environment.interface';

export const environment: IEnvironment = {
  ...prodEnvironment,
  dev: true,
  production: false,
  backEndUrl: 'https://cronicler.eu',
};
