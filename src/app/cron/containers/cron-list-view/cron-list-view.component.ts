import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CRON_LIST_TYPE, cronListTypes, ICron, ICronListPayload, ICronQuery } from '../../../../core/models/cron.interface';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { CronGetListAction, CronState, CronSwitchListType } from '../../../../core/store/cron.state';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IModalOptions } from '../../../../core/models/modal.interface';
import { HELP_KEY, HELP_TYPE } from '../../../../core/models/help.interface';

@Component({
  selector: 'app-cron-list-view',
  templateUrl: './cron-list-view.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronListViewComponent implements OnInit {
  @Select(CronState.cronListPayload) public cronListPayload$: Observable<ICronListPayload>;
  @Select(CronState.cronListType) public cronListType: Observable<CRON_LIST_TYPE>;
  public helpOptions: IModalOptions = {
    key: HELP_KEY.PUBLISH,
    type: HELP_TYPE.LIST,
  };

  public get cronTypeList(): CRON_LIST_TYPE[] {
    return cronListTypes;
  }

  constructor(private store: Store, private actions$: Actions) {}

  public ngOnInit(): void {
    this.getCronList();
    this.actions$.pipe(
      ofActionSuccessful(CronSwitchListType),
      tap((_) => this.getCronList()),
    );
  }

  public getCronList(cronQuery?: ICronQuery): void {
    this.store.dispatch(new CronGetListAction(cronQuery));
  }

  public onCronListTypeSwitch(cronListType: string): void {
    this.store.dispatch(new CronSwitchListType(cronListType as CRON_LIST_TYPE));
  }
}
