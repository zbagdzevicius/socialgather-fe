import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MAX_CONTENT_LENGTH, VALID_URL_REGEXP } from 'src/core/constants/cron.constants';
import { getMinDate } from '../../../../core/constants/time.constants';
import { CRON_POST_TYPE, cronPostTypes } from 'src/core/models/cron.interface';
import {
  ICroniclerImage,
  ICroniclerImageSearch,
  IFileUploadResponse,
  IImageToUpload,
} from '../../../../core/models/cronicler-image.interface';
import { ModalService } from 'src/core/services/modal/modal.service';
import { SnackBarOpenAction } from 'src/core/store/app.state';

import { HELP_KEY, HELP_TYPE } from '../../../../core/models/help.interface';
import { IModalOptions } from '../../../../core/models/modal.interface';
import { CronGetListAction, CronPostAction, CronState, SetCronPostTypeAction } from '../../../../core/store/cron.state';
import {
  ImageClearAction,
  ImageGetAction,
  ImagesClearAction,
  ImageSelectAction,
  ImageState,
  ImageUploadAction,
  ImageUploadSelectAction,
} from '../../../../core/store/image.state';

@Component({
  selector: 'app-cron-edit',
  templateUrl: './cron-edit.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronEditComponent implements OnInit, OnDestroy {
  @Select(ImageState.selectedImage) public image: Observable<ICroniclerImage>;
  @Select(ImageState.images) public images: Observable<ICroniclerImage[]>;
  @Select(CronState.cronPostType) public cronPostType$: Observable<CRON_POST_TYPE>;
  public helpOptions: IModalOptions = {
    key: HELP_KEY.PUBLISH,
    type: HELP_TYPE.CREATE,
  };
  public form: FormGroup;
  public selectedImageSubscription: Subscription;
  public imageSearchResultSubscription: Subscription;
  public scheduledPostSubscription: Subscription;
  public imageUploadSubscription: Subscription;
  public cronPostTypeChangeSubscription: Subscription;
  public validatorsByCronPostType: object = {
    [CRON_POST_TYPE.IMAGE]: {
      imageUrl: [Validators.required, Validators.pattern(VALID_URL_REGEXP)],
      linkUrl: [Validators.pattern(VALID_URL_REGEXP), Validators.pattern(VALID_URL_REGEXP)],
      content: [Validators.maxLength(MAX_CONTENT_LENGTH)],
    },
    [CRON_POST_TYPE.LINK]: {
      imageUrl: [],
      linkUrl: [Validators.pattern(VALID_URL_REGEXP), Validators.required],
      content: [Validators.maxLength(MAX_CONTENT_LENGTH)],
    },
    [CRON_POST_TYPE.MESSAGE]: {
      imageUrl: [],
      linkUrl: [Validators.pattern(VALID_URL_REGEXP)],
      content: [Validators.maxLength(MAX_CONTENT_LENGTH), Validators.required],
    },
  };

  public get cronPostTypes(): CRON_POST_TYPE[] {
    return cronPostTypes;
  }

  constructor(
    private _fb: FormBuilder,
    private _store: Store,
    private _actions$: Actions,
    private _modalService: ModalService,
    private _cdr: ChangeDetectorRef,
  ) {}

  public ngOnInit(): void {
    this.form = this._fb.group({
      imageUrl: [null],
      file: [null],
      linkUrl: [null],
      content: [null],
      postOn: [getMinDate().toISOString(), Validators.required],
    });
    this.updateFormValidatorsBasedOnPostType();
    this.reloadScheduledPostsOnNewPost();
    this.watchImageSearchResultToUpdateForm();
    this.watchImageUploadProcess();
    this.openImageSelectModalOnImageSelectWithLoadedImages();
  }

  public ngOnDestroy(): void {
    this.selectedImageSubscription?.unsubscribe();
    this.imageSearchResultSubscription?.unsubscribe();
    this.scheduledPostSubscription?.unsubscribe();
    this.imageUploadSubscription?.unsubscribe();
    this.cronPostTypeChangeSubscription?.unsubscribe();
    this._store.dispatch(new ImagesClearAction());
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      this._store.dispatch(new SnackBarOpenAction('Form is invalid. Please check all required fields'));
      return;
    }
    if (this._store.selectSnapshot(CronState.cronPostType) === CRON_POST_TYPE.IMAGE) {
      this._store.dispatch(new ImageUploadAction());
    } else {
      this.submitForm();
    }
  }

  public submitForm(): void {
    const { value, valid } = this.form;
    if (valid) {
      this._store.dispatch(new CronPostAction(value));
    } else {
      this._store.dispatch(new SnackBarOpenAction('Not all required fields are filled'));
    }
  }

  public reloadScheduledPostsOnNewPost(): void {
    this.scheduledPostSubscription = this._actions$
      .pipe(
        ofActionSuccessful(CronPostAction),
        tap(() => {
          this._store.dispatch(new CronGetListAction());
        }),
      )
      .subscribe();
  }

  public openImageSelectModalOnImageSelectWithLoadedImages(): void {
    this.imageSearchResultSubscription = this.images.subscribe(async (images) => {
      if (images?.length) {
        const modal: HTMLIonModalElement = await this._modalService.openImageSelectModal({
          images,
        });
        await modal.onWillDismiss().then((payload: any) => {
          const { data } = payload;
          this._store.dispatch(new ImageSelectAction(data.image as ICroniclerImage));
        });
      }
    });
  }

  public onImageSearch(imageSearch: ICroniclerImageSearch): void {
    this._store.dispatch(new ImageGetAction(imageSearch));
  }

  public onImageUploadSelect(imageToUpload: IImageToUpload): void {
    this._store.dispatch(new ImageUploadSelectAction(imageToUpload));
  }

  public onImageClear(): void {
    this._store.dispatch(new ImageClearAction());
  }

  public watchImageSearchResultToUpdateForm(): void {
    this.selectedImageSubscription = this.image.subscribe((image) => this.form.patchValue({ imageUrl: image?.thumbnailImageUrl || null }));
  }

  public watchImageUploadProcess(): void {
    this.imageUploadSubscription = this._actions$
      .pipe(
        ofActionSuccessful(ImageUploadAction),
        tap((_) => {
          const imageUploadResponse: IFileUploadResponse = this._store.selectSnapshot(ImageState.imageUploadResponse);
          if (imageUploadResponse?.success) {
            this.form.get('imageUrl').patchValue(imageUploadResponse.imageUrl);
            this.submitForm();
          } else {
            this._store.dispatch(new SnackBarOpenAction('Image upload failed'));
          }
        }),
      )
      .subscribe();
  }

  public updateFormValidatorsBasedOnPostType(): void {
    this.cronPostTypeChangeSubscription = this.cronPostType$.subscribe((value) => {
      const currentCronPostTypeValidators: object = this.validatorsByCronPostType[value];
      Object.keys(currentCronPostTypeValidators).forEach((key) => {
        const validators: ValidatorFn[] = currentCronPostTypeValidators[key];
        if (validators?.length) {
          this.form.get(key).setValidators(validators);
        } else {
          this.form.get(key).clearValidators();
        }
      });
      this._cdr.detectChanges();
    });
  }

  public onCronPostTypeChange(cronPostType: CRON_POST_TYPE): void {
    this._store.dispatch(new SetCronPostTypeAction(cronPostType));
  }

  public onImageUrlInput(imageUrl: string): void {
    this.form.get('imageUrl').patchValue(imageUrl);
    if (this.form.get('imageUrl').valid) {
      this._store.dispatch(
        new ImageSelectAction({
          fullImageUrl: imageUrl,
          thumbnailImageUrl: imageUrl,
          query: null,
        }),
      );
    } else {
      this.onImageClear();
    }
  }
}
