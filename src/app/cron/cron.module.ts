import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CronEditComponent } from './containers/cron-edit/cron-edit.component';
import { CronFormComponent } from './components/cron-form/cron-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { CRON_ROUTES } from './cron.routes';
import { CronListComponent } from './components/cron-list/cron-list.component';
import { CronListViewComponent } from './containers/cron-list-view/cron-list-view.component';
import { DragDropDirective } from './directives/drag-drop.directive';
import { ImageUploadComponent } from './components/image-upload/image-upload.component';
import { NgxsModule } from '@ngxs/store';
import { CronState } from '../../core/store/cron.state';
import { CronListTypeSwitcherComponent } from './components/cron-list-type-switcher/cron-list-type-switcher.component';
import { CronViewComponent } from './views/cron-view/cron-view.component';
import { ImageFinderComponent } from './components/image-finder/image-finder.component';
import { ImageState } from '../../core/store/image.state';

const DIRECTIVES: any = [DragDropDirective];

@NgModule({
  declarations: [
    ...DIRECTIVES,
    CronEditComponent,
    CronFormComponent,
    CronListComponent,
    CronListViewComponent,
    ImageUploadComponent,
    CronListTypeSwitcherComponent,
    CronViewComponent,
    ImageFinderComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    NgxsModule.forFeature([CronState, ImageState]),
    RouterModule.forChild(CRON_ROUTES),
  ],
})
export class CronModule {}
