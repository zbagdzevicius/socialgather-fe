import { Directive, Output, EventEmitter, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDragDrop]',
})
export class DragDropDirective {
  @Output() public fileDrop: EventEmitter<FileList> = new EventEmitter<FileList>();
  @HostBinding('class.dragover') public dragover: boolean;

  @HostListener('dragover', ['$event']) public onDragOver(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.dragover = true;
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.dragover = false;
  }

  @HostListener('drop', ['$event']) public ondrop(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.dragover = false;

    const files: FileList = event.dataTransfer.files;
    if (files.length > 0) {
      this.fileDrop.emit(files);
    }
  }
}
