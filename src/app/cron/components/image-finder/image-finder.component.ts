import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ICroniclerImage, ICroniclerImageSearch, IImageToUpload, IMAGE_PROVIDERS } from 'src/core/models/cronicler-image.interface';
import { ImageUploadComponent } from '../image-upload/image-upload.component';

@Component({
  selector: 'app-image-finder',
  templateUrl: './image-finder.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageFinderComponent implements OnInit, OnDestroy {
  @Input() public image: ICroniclerImage;
  @Output() public imageSearch: EventEmitter<ICroniclerImageSearch> = new EventEmitter<ICroniclerImageSearch>();
  @Output() public imageUploadSelect: EventEmitter<IImageToUpload> = new EventEmitter<IImageToUpload>();
  @Output() public imageClear: EventEmitter<void> = new EventEmitter<void>();
  @Output() public imageUrlInput: EventEmitter<SafeResourceUrl> = new EventEmitter<SafeResourceUrl>();
  @ViewChild(ImageUploadComponent) public imageUpload: ImageUploadComponent;
  public form: FormGroup;
  public imageSearchProviders: IMAGE_PROVIDERS[] = [
    IMAGE_PROVIDERS.PEXELS,
    IMAGE_PROVIDERS.UNSPLASH,
    IMAGE_PROVIDERS.IMAGE_UPLOAD,
    IMAGE_PROVIDERS.IMAGE_URL,
  ];
  public imageUrlChangeSubscription: Subscription;

  constructor(private _fb: FormBuilder, private _domSanitizer: DomSanitizer) {}

  public get imageProvider(): typeof IMAGE_PROVIDERS {
    return IMAGE_PROVIDERS;
  }

  public ngOnInit(): void {
    this.form = this._fb.group({
      query: ['', Validators.required],
      imageProvider: [IMAGE_PROVIDERS.IMAGE_UPLOAD, Validators.required],
      imageUrl: [null],
    });
    this.onImageUrlInput();
  }

  public ngOnDestroy(): void {
    this.imageUrlChangeSubscription?.unsubscribe();
  }

  public onImageSearch(): void {
    const { value, valid } = this.form;
    if (valid) {
      this.imageSearch.emit({ query: value.query, provider: value.imageProvider });
    }
  }

  public onImageClear(): void {
    if (this.imageUpload?.inputRef) {
      this.imageUpload.inputRef.nativeElement.value = null;
    }
    this.imageClear.emit();
  }

  public onImageUrlInput(): void {
    this.imageUrlChangeSubscription = this.form
      .get('imageUrl')
      .valueChanges.pipe(debounceTime(2000))
      .subscribe((value: string) => this.imageUrlInput.emit(value as any));
  }
}
