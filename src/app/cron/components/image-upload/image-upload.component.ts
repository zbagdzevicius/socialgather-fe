import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Store } from '@ngxs/store';
import { IImageToUpload } from 'src/core/models/cronicler-image.interface';
import { SnackBarOpenAction } from 'src/core/store/app.state';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageUploadComponent {
  @Output() public imageUploadSelect: EventEmitter<IImageToUpload> = new EventEmitter<IImageToUpload>();
  @ViewChild('imageUpload') public inputRef: ElementRef;

  constructor(private _cdr: ChangeDetectorRef, public _store: Store) {}

  public handleFileUpload(eventTarget: HTMLInputElement): void {
    const file: File = eventTarget.files[0];
    if (!file) {
      this._resetFileUpload();
      this._store.dispatch(new SnackBarOpenAction('File not selected, previously selected image is cleared'));
      return;
    }
    const reader: FileReader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = () => {
      const blob: Blob = new Blob([new Uint8Array(reader.result as ArrayBuffer)]);
      const blobURL: string = URL.createObjectURL(blob);
      this._cdr.markForCheck();
      this.imageUploadSelect.emit({
        blob,
        imageUrl: blobURL,
      });
      this._store.dispatch(new SnackBarOpenAction('Image selected successfully'));
    };

    reader.onerror = (error) => {
      this._resetFileUpload();
      this._store.dispatch(new SnackBarOpenAction(`Something went wrong, previously selected image is cleared: ${error}`));
    };
  }

  private _resetFileUpload(): void {
    this.inputRef.nativeElement.value = null;
    this.imageUploadSelect.emit(null);
  }
}
