import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CRON_POST_TYPE } from 'src/core/models/cron.interface';
import { ICroniclerImage, ICroniclerImageSearch, IImageToUpload } from 'src/core/models/cronicler-image.interface';

@Component({
  selector: 'app-cron-form',
  templateUrl: './cron-form.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronFormComponent {
  @Input() public form: FormGroup;
  @Input() public image: ICroniclerImage;
  @Input() public cronPostType: CRON_POST_TYPE;
  @Output() public submitEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() public imageSearch: EventEmitter<ICroniclerImageSearch> = new EventEmitter<ICroniclerImageSearch>();
  @Output() public imageUploadSelect: EventEmitter<IImageToUpload> = new EventEmitter<IImageToUpload>();
  @Output() public imageClear: EventEmitter<void> = new EventEmitter<void>();
  @Output() public imageUrlInput: EventEmitter<string> = new EventEmitter<string>();
  public contentPlaceholderByCronPostType: object = {
    [CRON_POST_TYPE.IMAGE]: 'Image caption',
    [CRON_POST_TYPE.LINK]: 'Link description',
    [CRON_POST_TYPE.MESSAGE]: 'Post message',
  };

  public get availableCronPostType(): typeof CRON_POST_TYPE {
    return CRON_POST_TYPE;
  }
}
