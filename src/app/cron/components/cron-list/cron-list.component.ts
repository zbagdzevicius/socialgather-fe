import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ICron } from '../../../../core/models/cron.interface';

@Component({
  selector: 'app-cron-list',
  templateUrl: './cron-list.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronListComponent {
  @Input() public cronList: ICron[];
}
