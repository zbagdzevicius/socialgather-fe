import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { CRON_LIST_TYPE } from '../../../../core/models/cron.interface';

@Component({
  selector: 'app-cron-list-type-switcher',
  templateUrl: './cron-list-type-switcher.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronListTypeSwitcherComponent {
  @Output() public cronListTypeSwitch: EventEmitter<CRON_LIST_TYPE> = new EventEmitter<CRON_LIST_TYPE>();
  @Input() public cronListType: CRON_LIST_TYPE;
  public cronListTypes: CRON_LIST_TYPE[] = [CRON_LIST_TYPE.UPCOMING, CRON_LIST_TYPE.PAST];
}
