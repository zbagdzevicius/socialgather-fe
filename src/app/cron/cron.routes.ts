import { Routes } from '@angular/router';
import { CronViewComponent } from './views/cron-view/cron-view.component';

export const CRON_ROUTES: Routes = [
  {
    path: '',
    component: CronViewComponent,
  },
  {
    path: '*',
    redirectTo: '',
  },
];
