import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { StatisticsGetAction, StatisticsState, StatisticsSwitchCategoryAction } from '../../../../core/store/statistics.state';
import { Observable } from 'rxjs';
import { IStatistics, STATISTICS_CATEGORY } from '../../../../core/models/statistics.interface';
import { IModalOptions } from '../../../../core/models/modal.interface';
import { HELP_KEY, HELP_TYPE } from '../../../../core/models/help.interface';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatisticsComponent implements OnInit {
  @Select(StatisticsState.statistics) public statistics$: Observable<IStatistics>;
  @Select(StatisticsState.category) public category$: Observable<STATISTICS_CATEGORY>;
  public helpOptions: IModalOptions = {
    key: HELP_KEY.STATISTICS,
    type: HELP_TYPE.LIST,
  };
  constructor(private store: Store) {}

  public ngOnInit(): void {
    this.store.dispatch(new StatisticsGetAction());
  }

  public onStatisticsCategorySwitch(statisticsCategory: STATISTICS_CATEGORY): void {
    this.store.dispatch(new StatisticsSwitchCategoryAction(statisticsCategory));
  }
}
