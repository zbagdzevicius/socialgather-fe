import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { RouterModule } from '@angular/router';
import { StatisticsComponent } from './containers/statistics/statistics.component';
import { STATISTICS_ROUTES } from './statistics.routes';
import { LayoutModule } from '../layout/layout.module';
import { NgxsModule } from '@ngxs/store';
import { StatisticsState } from '../../core/store/statistics.state';
import { StatisticsMetricsComponent } from './components/statistics-metrics/statistics-metrics.component';
import { StatisticsTabsComponent } from './components/statistics-tabs/statistics-tabs.component';
import { FansLocationsComponent } from './components/fans-locations/fans-locations.component';

@NgModule({
  declarations: [StatisticsMetricsComponent, StatisticsComponent, StatisticsTabsComponent, FansLocationsComponent],
  imports: [CommonModule, RouterModule.forChild(STATISTICS_ROUTES), SharedModule, LayoutModule, NgxsModule.forFeature([StatisticsState])],
})
export class StatisticsModule {}
