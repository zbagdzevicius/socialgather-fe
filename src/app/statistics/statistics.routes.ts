import { Routes } from '@angular/router';
import { StatisticsComponent } from './containers/statistics/statistics.component';

export const STATISTICS_ROUTES: Routes = [
  {
    path: '',
    component: StatisticsComponent,
  },
];
