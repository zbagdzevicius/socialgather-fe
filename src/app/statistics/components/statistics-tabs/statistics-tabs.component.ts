import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { IStatistics, STATISTICS_CATEGORY, statisticsCategories } from '../../../../core/models/statistics.interface';

@Component({
  selector: 'app-statistics-tabs',
  templateUrl: './statistics-tabs.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatisticsTabsComponent {
  @Input() public statistics: IStatistics;
  @Input() public statisticsCategory: STATISTICS_CATEGORY;
  @Output() public statisticsCategorySwitch: EventEmitter<STATISTICS_CATEGORY> = new EventEmitter<STATISTICS_CATEGORY>();

  public get statisticsCategories(): STATISTICS_CATEGORY[] {
    return statisticsCategories;
  }

  public onStatisticsCategorySwitch(statisticsCategory: string): void {
    this.statisticsCategorySwitch.emit(statisticsCategory as STATISTICS_CATEGORY);
  }
}
