import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { IMetricsCount } from '../../../../core/models/statistics.interface';
import { TickOptions, Chart, ChartConfiguration, NestedTickOptions } from 'chart.js';

export interface ChartAxes {
  labelList: Date[] | string[];
  countList: number[];
}

@Component({
  selector: 'app-statistics-metrics',
  templateUrl: './statistics-metrics.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatisticsMetricsComponent implements OnChanges {
  @Input() public metrics: IMetricsCount[];

  @ViewChild('lineChart', { static: true }) private chartRef: ElementRef;

  public chartAxes: ChartAxes;
  public chart: Chart;
  public resizeTimeout: any;
  public chartStyle: Partial<NestedTickOptions> = {
    fontFamily: 'Raleway',
    fontColor: '#271c6b',
    fontSize: 14,
  };
  public chartOptions: ChartConfiguration = {
    type: 'line',
    options: {
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            display: true,
            gridLines: {
              display: false,
            },
            ticks: {
              ...this.chartStyle,
            },
          },
        ],
        yAxes: [
          {
            display: true,
            ticks: {
              ...this.chartStyle,
              suggestedMin: 0,
              precision: 0,
              callback: (yAxesValue) => {
                if (yAxesValue !== 0) {
                  return yAxesValue;
                }
              },
            } as TickOptions,
          },
        ],
      },
    },
  };

  @HostListener('window:resize')
  public reinitializeChart(): void {
    if (this.resizeTimeout) {
      clearTimeout(this.resizeTimeout);
    }
    this.resizeTimeout = setTimeout(
      (() => {
        if (this.metrics) {
          this.chart.destroy();
          this.updateChart(this.metrics);
        }
      }).bind(this),
      250,
    );
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.metrics && this.metrics) {
      this.updateChart(this.metrics);
    }
  }

  public updateChart(metrics: IMetricsCount[]): void {
    this.chartAxes = this.splitMetricsIntoAxes(metrics);
    this.chart = this.getChart(this.chartAxes);
  }

  public splitMetricsIntoAxes(metrics: IMetricsCount[]): ChartAxes {
    const labelList: string[] = [];
    const countList: number[] = [];
    metrics.forEach((metric) => {
      labelList.push(metric.date.toDateString());
      countList.push(metric.count);
    });

    return {
      labelList,
      countList,
    };
  }

  public getChart(chartAxes: ChartAxes): Chart {
    return new Chart(this.chartRef.nativeElement, {
      ...this.chartOptions,
      data: {
        labels: chartAxes.labelList,
        datasets: [
          {
            data: chartAxes.countList,
            backgroundColor: this.getChartDatasetGradient(),
            borderColor: '#13023f',
          },
        ],
      },
    });
  }

  public getChartDatasetGradient(): CanvasGradient {
    const ctx: CanvasRenderingContext2D = this.chartRef.nativeElement.getContext('2d');
    const gradient: CanvasGradient = ctx.createLinearGradient(0, 0, 0, 450);
    gradient.addColorStop(0, 'rgba(19, 2, 63, 0.2)');
    gradient.addColorStop(1, 'rgba(55, 67, 152, 0)');
    return gradient;
  }
}
