import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { IFansLocation } from '../../../../core/models/statistics.interface';
import { ChartAxes } from '../statistics-metrics/statistics-metrics.component';
import { Chart, ChartConfiguration, NestedTickOptions, TickOptions } from 'chart.js';

@Component({
  selector: 'app-fans-locations',
  templateUrl: './fans-locations.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FansLocationsComponent implements OnChanges {
  @Input() public fansLocationList: IFansLocation[];
  @ViewChild('pieChart', { static: true }) private chartRef: ElementRef;

  public chartAxes: ChartAxes;
  public chart: Chart;
  public resizeTimeout: any;
  public chartStyle: Partial<NestedTickOptions> = {
    fontFamily: 'Raleway',
    fontColor: '#271c6b',
    fontSize: 18,
  };
  public chartOptions: ChartConfiguration = {
    type: 'doughnut',
    options: {
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            display: true,
            gridLines: {
              display: false,
            },
            ticks: {
              ...this.chartStyle,
            },
          },
        ],
        yAxes: [
          {
            display: false,
            ticks: {
              ...this.chartStyle,
              suggestedMin: 0,
              precision: 0,
              callback: (yAxesValue) => {
                if (yAxesValue !== 0) {
                  return yAxesValue;
                }
              },
            } as TickOptions,
          },
        ],
      },
    },
  };

  @HostListener('window:resize')
  public reinitializeChart(): void {
    if (this.resizeTimeout) {
      clearTimeout(this.resizeTimeout);
    }
    this.resizeTimeout = setTimeout(
      (() => {
        if (this.fansLocationList) {
          this.chart.destroy();
          this.updateChart(this.fansLocationList);
        }
      }).bind(this),
      250,
    );
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.fansLocationList) {
      this.updateChart(this.fansLocationList);
    }
  }

  public updateChart(metrics: IFansLocation[]): void {
    this.chartAxes = this.splitMetricsIntoAxes(metrics);
    this.chart = this.getChart(this.chartAxes);
  }

  public splitMetricsIntoAxes(metrics: IFansLocation[]): ChartAxes {
    const labelList: string[] = [];
    const countList: number[] = [];
    metrics.forEach((metric) => {
      labelList.push(metric.location);
      countList.push(metric.fans);
    });

    return {
      labelList,
      countList,
    };
  }

  public getChart(chartAxes: ChartAxes): Chart {
    return new Chart(this.chartRef.nativeElement, {
      ...this.chartOptions,
      data: {
        labels: chartAxes.labelList,
        datasets: [
          {
            data: chartAxes.countList,
            backgroundColor: this.getChartDatasetGradient(),
            borderColor: '#13023f',
          },
        ],
      },
    });
  }

  public getChartDatasetGradient(): CanvasGradient {
    const ctx: CanvasRenderingContext2D = this.chartRef.nativeElement.getContext('2d');
    const gradient: CanvasGradient = ctx.createLinearGradient(0, 0, 0, 450);
    gradient.addColorStop(0, 'rgba(19, 2, 63, 0.2)');
    gradient.addColorStop(1, 'rgba(55, 67, 152, 0)');
    return gradient;
  }
}
