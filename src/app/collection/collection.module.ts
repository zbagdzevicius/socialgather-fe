import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollectionCreateComponent } from './containers/collection-create/collection-create.component';
import { CollectionEditComponent } from './containers/collection-edit/collection-edit.component';
import { CollectionSelectComponent } from './containers/collection-select/collection-select.component';
import { RouterModule } from '@angular/router';
import { COLLECTION_ROUTES } from './collection.routes';
import { NgxsModule } from '@ngxs/store';
import { CollectionFormComponent } from './components/collection-form/collection-form.component';
import { CollectionState } from '../../core/store/collection.state';
import { CollectionListComponent } from './components/collection-list/collection-list.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CollectionSetupComponent } from './views/collection-setup/collection-setup.component';
import { SocialNetworkModule } from '../social-network/social-network.module';
import { SocialUnitModule } from '../social-unit/social-unit.module';

@NgModule({
  declarations: [
    CollectionCreateComponent,
    CollectionEditComponent,
    CollectionSelectComponent,
    CollectionFormComponent,
    CollectionListComponent,
    CollectionSetupComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(COLLECTION_ROUTES),
    NgxsModule.forFeature([CollectionState]),
    SharedModule,
    ReactiveFormsModule,
    SocialNetworkModule,
    SocialUnitModule,
  ],
})
export class CollectionModule {}
