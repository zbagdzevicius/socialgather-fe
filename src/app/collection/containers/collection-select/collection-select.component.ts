import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { ICollection } from '../../../../core/models/collection.interface';
import { Observable, Subscription } from 'rxjs';
import { CollectionState, CollectionGetListAction, CollectionSwitchAction } from '../../../../core/store/collection.state';
import { GoToCollectionCreateAction, GoToDashboardAction } from '../../../../core/store/route.state';
import { filter, tap } from 'rxjs/operators';
import { IModalOptions } from '../../../../core/models/modal.interface';
import { HELP_KEY, HELP_TYPE } from '../../../../core/models/help.interface';
import { AuthState } from 'src/core/store/auth.state';

@Component({
  selector: 'app-collection-select',
  templateUrl: './collection-select.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionSelectComponent implements OnInit, OnDestroy {
  @Select(CollectionState.collectionList) public collectionList$: Observable<ICollection[]>;
  @Select(CollectionState.collection) public collection$: Observable<ICollection>;
  @Select(AuthState.isAuthenticated) public isAuthenticated$: Observable<boolean>;
  public helpOptions: IModalOptions = {
    key: HELP_KEY.COLLECTION,
    type: HELP_TYPE.SELECT,
  };
  private _subscription: Subscription = new Subscription();

  constructor(private _store: Store, private _actions: Actions) {
    this.collectionSwitchSubscribe();
  }

  public ngOnInit(): void {
    const loadOnlyIfAuthenticatedSubscription: Subscription = this.isAuthenticated$
      .pipe(
        filter((isAuthenticated) => isAuthenticated),
        tap(() => this._store.dispatch(new CollectionGetListAction())),
      )
      .subscribe();
    this._subscription.add(loadOnlyIfAuthenticatedSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  public onCollectionCreateButtonClick(): void {
    this._store.dispatch(new GoToCollectionCreateAction());
  }

  public onCollectionSelect(collection: ICollection): void {
    this._store.dispatch(new CollectionSwitchAction(collection));
  }

  public collectionSwitchSubscribe(): void {
    const collectionSwitchSubscription: Subscription = this._actions
      .pipe(
        ofActionSuccessful(CollectionSwitchAction),
        tap((_) => this._store.dispatch(new GoToDashboardAction())),
      )
      .subscribe();
    this._subscription.add(collectionSwitchSubscription);
  }
}
