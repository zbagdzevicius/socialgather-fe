import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { CollectionCreateAction } from '../../../../core/store/collection.state';
import { SnackBarOpenAction } from '../../../../core/store/app.state';
import { GoToCollectionSelectAction } from '../../../../core/store/route.state';

@Component({
  selector: 'app-collection-edit',
  templateUrl: './collection-edit.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionEditComponent implements OnInit {
  public form: FormGroup;

  constructor(private store: Store, private fb: FormBuilder) {}

  public ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      socialNetworks: [[], Validators.required],
    });
  }

  public onSubmit(): void {
    const { value, valid } = this.form;
    if (valid) {
      this.store.dispatch(new CollectionCreateAction(value));
    } else {
      this.store.dispatch(new SnackBarOpenAction('Collection is not valid'));
    }
  }

  public onCancel(): void {
    this.store.dispatch(new GoToCollectionSelectAction());
  }
}
