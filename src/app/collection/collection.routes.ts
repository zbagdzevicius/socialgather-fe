import { Routes } from '@angular/router';
import { CollectionSelectComponent } from './containers/collection-select/collection-select.component';
import { CollectionCreateComponent } from './containers/collection-create/collection-create.component';
import { CollectionEditComponent } from './containers/collection-edit/collection-edit.component';
import { CollectionSetupComponent } from './views/collection-setup/collection-setup.component';
import { CollectionGuard } from '../../core/guards/collection.guard';

export const COLLECTION_ROUTES: Routes = [
  {
    path: '',
    component: CollectionSelectComponent,
  },
  {
    path: 'create',
    component: CollectionCreateComponent,
  },
  {
    path: 'edit/:id',
    component: CollectionEditComponent,
  },
  {
    path: 'setup',
    component: CollectionSetupComponent,
    canActivate: [CollectionGuard],
  },
];
