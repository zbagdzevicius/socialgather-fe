import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { ICollection } from '../../../../core/models/collection.interface';

@Component({
  selector: 'app-collection-list',
  templateUrl: './collection-list.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionListComponent {
  @Input() public collectionList: ICollection[] = [];
  @Input() public currentCollection: ICollection;
  @Output() public collectionCreateButtonClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() public collectionSelect: EventEmitter<ICollection> = new EventEmitter<ICollection>();
}
