import { Routes } from '@angular/router';
import { RegistrationComponent } from './containers/registration/registration.component';
import { LoginComponent } from './containers/login/login.component';
import { SocialLoginVerifyComponent } from './containers/social-login-verify/social-login-verify.component';

export const AUTH_ROUTES: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'registration',
    component: RegistrationComponent,
  },
  {
    path: 'social-login-verify/:accessToken',
    component: SocialLoginVerifyComponent,
  },
  {
    path: '',
    redirectTo: 'login',
  },
];
