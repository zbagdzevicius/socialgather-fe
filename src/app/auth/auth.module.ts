import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AUTH_ROUTES } from './auth.routes';
import { LoginComponent } from './containers/login/login.component';
import { RegistrationComponent } from './containers/registration/registration.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { NgxsModule } from '@ngxs/store';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthState } from '../../core/store/auth.state';
import { AuthService } from '../../core/services/auth/auth.service';
import { RoutingState } from 'src/core/store/route.state';

const COMPONENTS: any[] = [LoginComponent, RegistrationComponent, LoginFormComponent, RegistrationFormComponent];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(AUTH_ROUTES),
    NgxsModule.forFeature([AuthState, RoutingState]),
  ],
  providers: [AuthService],
})
export class AuthModule {}
