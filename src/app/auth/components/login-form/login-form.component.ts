import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SOCIAL_NETWORK_TYPE, socialNetworkProviderList } from '../../../../core/models/social-network.interface';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
})
export class LoginFormComponent {
  @Input() public form: FormGroup;

  @Output() public login: EventEmitter<void> = new EventEmitter<void>();
  @Output() public socialLogin: EventEmitter<SOCIAL_NETWORK_TYPE> = new EventEmitter<SOCIAL_NETWORK_TYPE>();

  public get socialLoginNetworkTypesList(): SOCIAL_NETWORK_TYPE[] {
    return socialNetworkProviderList.filter((provider) => provider.loginEnabled).map((provider) => provider.socialNetworkType);
  }
}
