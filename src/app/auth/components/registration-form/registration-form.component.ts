import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
})
export class RegistrationFormComponent {
  @Input() public form: FormGroup;

  @Output() public registration: EventEmitter<void> = new EventEmitter<void>();
  @Output() public goToLoginClick: EventEmitter<void> = new EventEmitter<void>();
}
