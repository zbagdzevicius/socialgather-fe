import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { IUserLoginForm } from '../../../../core/models/user.interface';
import { LoginAction, SocialLoginAction } from '../../../../core/store/auth.state';
import { SOCIAL_NETWORK_TYPE } from '../../../../core/models/social-network.interface';
import { GoToRegistrationAction } from '../../../../core/store/route.state';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  public form: FormGroup;

  constructor(private _store: Store) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl(null, Validators.email),
      password: new FormControl(null),
    });
  }

  public login(): void {
    if (this.form.valid) {
      this._store.dispatch(new LoginAction(this.form.value as IUserLoginForm));
    }
  }

  public socialLogin(socialNetworkType: SOCIAL_NETWORK_TYPE): void {
    this._store.dispatch(new SocialLoginAction(socialNetworkType));
  }

  public onGoToRegistrationClick(): void {
    this._store.dispatch(new GoToRegistrationAction());
  }
}
