import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { SocialLoginVerifyAction } from 'src/core/store/auth.state';

@Component({
  selector: 'app-social-login-verify',
  templateUrl: './social-login-verify.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocialLoginVerifyComponent implements OnInit {
  constructor(private _store: Store, private _route: ActivatedRoute) {}

  public ngOnInit(): void {
    const accessToken: string = this._route.snapshot.paramMap.get('accessToken');
    this._store.dispatch(new SocialLoginVerifyAction(accessToken));
  }
}
