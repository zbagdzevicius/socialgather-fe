import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { IUserRegistrationForm } from '../../../../core/models/user.interface';
import { RegistrationAction } from '../../../../core/store/auth.state';
import { GoToLoginAction, GoToPrivacyPolicy } from '../../../../core/store/route.state';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
})
export class RegistrationComponent implements OnInit {
  public form: FormGroup;

  constructor(private store: Store) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl(null, [Validators.email, Validators.required]),
      password: new FormControl(null, Validators.required),
      passwordRepeat: new FormControl(null, Validators.required),
    });
  }

  public registration(): void {
    const { value, valid } = this.form;
    if (valid) {
      this.store.dispatch(new RegistrationAction(value as IUserRegistrationForm));
    }
  }

  public onGoToLoginClick(): void {
    this.store.dispatch(new GoToLoginAction());
  }

  public onPrivacyPolicyClick(): void {
    this.store.dispatch(new GoToPrivacyPolicy());
  }
}
