import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { APP_ROUTES_WITH_LAYOUTS, FEATURES_ROUTES } from './app.routes';
import { AppState } from '../core/store/app.state';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsModule, NgxsModuleOptions } from '@ngxs/store';
import { environment } from '../environments/environment';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { LayoutModule } from './layout/layout.module';
import { RoutingState } from '../core/store/route.state';
import { APIInterceptor } from '../core/interceptors/API.interceptor';
import { ErrorInterceptor } from '../core/interceptors/error.interceptor';
import { ActivateIfLoggedInGuard } from '../core/guards/auth.guard';
import { ActivateIfNotLoggedInGuard } from '../core/guards/not-auth.guard';
import { AuthInterceptor } from '../core/interceptors/auth.interceptor';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { UserState } from '../core/store/user.state';
import { SupportModule } from './support/support.module';
import { SentryErrorHandler } from '../core/errors/sentry-error.handler';
import { IonicModule } from '@ionic/angular';
import { ModalComponent } from '../core/services/modal/modal.component';
import { EmailState } from 'src/core/store/email.state';

const ngxsOptions: NgxsModuleOptions = {
  developmentMode: !environment.production,
  selectorOptions: {
    // These Selector Settings are recommended in preparation for NGXS v4
    suppressErrors: false,
    injectContainerState: false,
  },
};

const NGXS_IMPORTS: any[] = [
  NgxsModule.forRoot([AppState, UserState], ngxsOptions),
  NgxsModule.forFeature([RoutingState, EmailState]),
  NgxsRouterPluginModule.forRoot(),
  NgxsStoragePluginModule.forRoot({
    key: ['auth.authenticated', 'collection.collection', 'user.user'],
  }),
  NgxsReduxDevtoolsPluginModule.forRoot({ disabled: environment.production }),
];

const GLOBAL_INTERCEPTORS: any[] = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: APIInterceptor,
    multi: true,
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true,
  },
  { provide: ErrorHandler, useClass: SentryErrorHandler },
];

const GUARDS: any[] = [ActivateIfLoggedInGuard, ActivateIfNotLoggedInGuard];

const GLOBAL_SERVICES: any[] = [];

@NgModule({
  declarations: [AppComponent, ModalComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    ...NGXS_IMPORTS,
    SharedModule,
    RouterModule.forRoot(APP_ROUTES_WITH_LAYOUTS(FEATURES_ROUTES), { relativeLinkResolution: 'legacy' }),
    LayoutModule,
    SupportModule,
    NgxsResetPluginModule.forRoot(),
    IonicModule.forRoot(),
  ],
  providers: [...GLOBAL_INTERCEPTORS, ...GLOBAL_SERVICES, ...GUARDS],
  bootstrap: [AppComponent],
})
export class AppModule {}
