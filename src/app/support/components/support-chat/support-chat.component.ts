import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { HELP_KEY, HELP_TYPE } from 'src/core/models/help.interface';
import { IEmailModalOptions, IModalOptions } from 'src/core/models/modal.interface';

@Component({
  selector: 'app-support-chat',
  templateUrl: './support-chat.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SupportChatComponent implements OnChanges {
  @Input() public email: string;

  public helpOptions: IModalOptions = {
    type: HELP_TYPE.LIST,
    key: HELP_KEY.SUPPORT,
  };

  public emailModalOptions: IEmailModalOptions = {
    emailForm: {
      sender: null,
      subject: null,
      message: null,
    },
  };

  public ngOnChanges(changes: SimpleChanges): void {
    const emailChanged: boolean = changes.email && changes.email.currentValue !== changes.email.previousValue;
    if (emailChanged) {
      this._setEmailModalOptionsSender();
    }
  }

  private _setEmailModalOptionsSender(): void {
    this.emailModalOptions.emailForm.sender = this.email;
  }
}
