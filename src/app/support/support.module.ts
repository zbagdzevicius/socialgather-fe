import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupportChatComponent } from './components/support-chat/support-chat.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SupportChatComponent],
  imports: [CommonModule, SharedModule, ReactiveFormsModule, FormsModule],
  exports: [SupportChatComponent],
})
export class SupportModule {}
