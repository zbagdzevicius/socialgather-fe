import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngxs/store';
import { GoToHomeAction } from 'src/core/store/route.state';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotFoundComponent {
  constructor(private _store: Store) {}

  public goToHome(): void {
    this._store.dispatch(new GoToHomeAction());
  }
}
