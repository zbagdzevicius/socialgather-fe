import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';
import { RoutingState } from 'src/core/store/route.state';
import { SharedModule } from '../shared/shared.module';

import { INFORMATION_ROUTES } from './information.routes';
import { NotFoundComponent } from './not-found/not-found.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const EXPORTED_COMPONENTS: any[] = [PrivacyPolicyComponent, NotFoundComponent];

@NgModule({
  declarations: [...EXPORTED_COMPONENTS],
  imports: [CommonModule, SharedModule, RouterModule.forChild(INFORMATION_ROUTES), NgxsModule.forFeature([RoutingState])],
})
export class InformationModule {}
