import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngxs/store';
import { GoToLoginAction } from 'src/core/store/route.state';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyPolicyComponent {
  constructor(private _store: Store) {}

  public goToLogin(): void {
    this._store.dispatch(new GoToLoginAction());
  }
}
