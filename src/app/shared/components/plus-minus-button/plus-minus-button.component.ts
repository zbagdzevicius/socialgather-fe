import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-plus-minus-button',
  templateUrl: './plus-minus-button.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlusMinusComponent {
  @Input() public text: string;
  @Output() public plus: EventEmitter<void> = new EventEmitter<void>();
  @Output() public minus: EventEmitter<void> = new EventEmitter<void>();
}
