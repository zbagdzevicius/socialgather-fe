import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input } from '@angular/core';
import { ICollection } from '../../../../core/models/collection.interface';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoComponent {
  @Input() public collection: ICollection;
}
