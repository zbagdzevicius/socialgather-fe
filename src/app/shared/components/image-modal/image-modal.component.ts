import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ICroniclerImage } from 'src/core/models/cronicler-image.interface';
import { IImageSelectModalOptions } from 'src/core/models/modal.interface';

@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageModalComponent {
  @Input() public imageSelectModalOptions: IImageSelectModalOptions;

  constructor(public modalController: ModalController) {}

  public async onCloseClick(): Promise<boolean> {
    return await this.modalController.dismiss({
      dismissed: true,
    });
  }

  public async onImageSelect(image: ICroniclerImage): Promise<boolean> {
    return await this.modalController.dismiss({
      dismissed: true,
      image,
    });
  }
}
