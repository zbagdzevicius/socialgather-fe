import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageComponent {
  constructor(public domSanitizer: DomSanitizer) {}
  @Input() public source: string;
  @Input() public round: boolean;
  @Input() public filled: boolean;
  @Input() public border: boolean;
}
