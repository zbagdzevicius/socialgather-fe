import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-more-button',
  templateUrl: './more-button.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoreButtonComponent {}
