import { ChangeDetectionStrategy, Component, forwardRef, NgZone, ViewChild, ViewEncapsulation } from '@angular/core';
import { take } from 'rxjs/operators';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { InputComponent } from '../input/input.component';
import { NgControl } from '@angular/forms';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextareaComponent extends InputComponent {
  constructor(private _ngZone: NgZone, public control: NgControl) {
    super(control);
  }

  @ViewChild('autosize') public autosize: CdkTextareaAutosize;
  public triggerResize(): void {
    this._ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
  }
}
