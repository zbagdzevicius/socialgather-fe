import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-line-card',
  templateUrl: './line-card.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LineCardComponent {
  @Input() public disabled: boolean;
  @Input() public enabled: boolean;
}
