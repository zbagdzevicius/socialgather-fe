import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TabsComponent implements OnInit {
  @Input() public type: string;
  @Input() public types: string[];
  @Input() public allEnabled: boolean = true;
  @Output() public typeClick: EventEmitter<string> = new EventEmitter<string>();

  public ngOnInit(): void {
    if (this.type) {
      return;
    }
    if (!this.allEnabled && this.types?.length) {
      this.type = this.types[0];
      this.typeClick.emit(this.type);
    }
  }
}
