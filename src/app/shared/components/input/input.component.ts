import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { NgControl } from '@angular/forms';

export type IInputType = 'text' | 'password' | 'checkbox';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent {
  @Input() public placeholder: string;
  @Input() public label: string;
  @Input() public type: IInputType = 'text';
  @Input() public autocomplete: 'off' | 'new-password';

  constructor(public control: NgControl) {}

  public get error(): string {
    const errorsKeys: string[] = Object.keys(this.control.control.errors);
    if (errorsKeys && errorsKeys.length) {
      return Object.keys(this.control.control.errors)[0].toUpperCase();
    }

    return null;
  }
}
