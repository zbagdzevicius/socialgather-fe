import { Component, Input } from '@angular/core';
import { IColor } from '../../../../core/models/theme.interface';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
})
export class ButtonComponent {
  @Input() public text: string;
  @Input() public type: 'button' | 'submit' = 'button';
  @Input() public disabled: boolean;
  @Input() public icon: string;
  @Input() public badgeColor: IColor = 'accent';
  @Input() public badgeText: string | number;
  @Input() public theme: 'primary' | 'secondary' = 'primary';
  @Input() public size: string;
}
