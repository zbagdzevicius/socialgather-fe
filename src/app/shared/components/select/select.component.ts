import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { InputChangeEventDetail } from '@ionic/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectComponent {
  @Input() public formControl: AbstractControl;
  @Input() public label: string;
  @Input() public options: string[];
  @Output() public valueChanges: EventEmitter<string> = new EventEmitter<string>();

  public onValueChanges(event: CustomEvent<InputChangeEventDetail>): void {
    const selectValue: string = event?.detail?.value;
    if (selectValue) {
      this.valueChanges.emit(selectValue);
    }
  }
}
