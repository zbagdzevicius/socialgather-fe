import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { ModalOpenAction, SnackBarOpenAction } from '../../../../core/store/app.state';
import { IEmailModalOptions, IModalOptions } from '../../../../core/models/modal.interface';
import { IICon } from '../icon/icon.component';
import { ModalService } from 'src/core/services/modal/modal.service';
import { EmailSendAction } from 'src/core/store/email.state';
import { ICroniclerEmail } from 'src/core/models/cronicler-email.interface';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelpComponent {
  @Input() public modalOptions: IModalOptions;
  @Input() public emailModalOptions: IEmailModalOptions;
  @Input() public icon: IICon = 'help';

  constructor(private _store: Store, private _modalService: ModalService) {}

  public async openModal(): Promise<void> {
    if (this.emailModalOptions) {
      this.openEmailModal();
    } else {
      this._store.dispatch(new ModalOpenAction(this.modalOptions));
    }
  }

  public async openEmailModal(): Promise<void> {
    const modal: HTMLIonModalElement = await this._modalService.openEmailModal(this.emailModalOptions);
    await modal.onWillDismiss().then((payload: any) => {
      const { data } = payload;
      if (!data) {
        return;
      }
      if (data.emailForm && data.valid) {
        this._store.dispatch(new EmailSendAction(data.emailForm as ICroniclerEmail));
      } else {
        this._store.dispatch(new SnackBarOpenAction('Some details is missing or validation occurred. Email is not sent.'));
      }
    });
  }
}
