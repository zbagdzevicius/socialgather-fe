import { ChangeDetectionStrategy, Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Store } from '@ngxs/store';
import { getMinDate, MAX_SCHEDULING_DATE, ONE_MINUTE_IN_MILLISECONDS } from 'src/core/constants/time.constants';
import { SnackBarOpenAction } from 'src/core/store/app.state';

@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateTimePickerComponent implements OnDestroy {
  public maxDate: string = MAX_SCHEDULING_DATE.toISOString();
  public validationIntervalFunction: ReturnType<typeof setInterval>;

  public constructor(public control: NgControl, private _store: Store) {
    this.validationIntervalFunction = setInterval(() => {
      this.invalidAndUpdatedDate(new Date(this.control.control?.value));
    }, ONE_MINUTE_IN_MILLISECONDS);
  }
  public get currentDate(): string {
    return new Date().toISOString();
  }

  public get minDate(): Date {
    return getMinDate();
  }

  public ngOnDestroy(): void {
    clearInterval(this.validationIntervalFunction);
  }

  public updateDate(timeUpdateInMilliseconds: number): void {
    const currentTimeValue: number = new Date(this.control.control.value).getTime();
    const newDate: Date = new Date(currentTimeValue + timeUpdateInMilliseconds);
    const newDateReachedMaxDate: boolean = newDate.getTime() > new Date(this.maxDate).getTime();
    if (newDateReachedMaxDate) {
      this._store.dispatch(new SnackBarOpenAction('Maximum date reached'));
      return;
    }
    if (this.invalidAndUpdatedDate(newDate)) {
      return;
    }
    this._store.dispatch(new SnackBarOpenAction(`scheduling time: ${newDate.toLocaleString()}`));
    this.control.control.patchValue(newDate.toISOString());
  }

  public invalidAndUpdatedDate(validationDate: Date): boolean {
    let dateUpdate: boolean = false;
    const dateIsInvalid: boolean = validationDate.getTime() - this.minDate.getTime() <= 0;

    if (dateIsInvalid) {
      this.control.control.patchValue(this.minDate.toISOString());
      dateUpdate = true;
      this._store.dispatch(new SnackBarOpenAction('Scheduling time updated to 15 minutes ahead from now'));
    }

    return dateUpdate;
  }
}
