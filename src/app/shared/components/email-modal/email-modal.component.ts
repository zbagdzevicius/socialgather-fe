import { ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { IEmailModalOptions } from 'src/core/models/modal.interface';

@Component({
  selector: 'app-image-modal',
  templateUrl: './email-modal.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmailModalComponent implements OnInit {
  @Input() public emailModalOptions: IEmailModalOptions;
  public form: FormGroup;

  constructor(public modalController: ModalController, private _fb: FormBuilder) {
    this.form = this._fb.group({
      sender: [null, [Validators.required, Validators.email]],
      subject: [null, [Validators.required, Validators.minLength(10)]],
      message: [null, [Validators.required, Validators.minLength(20), Validators.maxLength(1000)]],
    });
  }

  public ngOnInit(): void {
    this.form.patchValue(this.emailModalOptions.emailForm);
  }

  public async onCloseClick(): Promise<boolean> {
    return await this.modalController.dismiss({
      dismissed: true,
    });
  }

  public async onSendClick(): Promise<boolean> {
    return await this.modalController.dismiss({
      dismissed: true,
      emailForm: this.form.value,
      valid: this.form.valid,
    });
  }
}
