import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';

export interface IIonicIcons {
  [key: string]: IIonIcon;
}

export type IICon =
  | 'help'
  | 'settings'
  | 'notifications'
  | 'support'
  | 'facebook'
  | 'twitter'
  | 'menuItem'
  | 'expiration'
  | 'close'
  | 'profile'
  | 'exit'
  | 'selectedCollection'
  | 'menuStatistics'
  | 'menuCollections'
  | 'menuSetup'
  | 'menuPublish'
  | 'plus'
  | 'minus';

// list of available icons https://ionicons.com/
export type IIonIcon =
  | 'settings-outline'
  | 'help-circle-outline'
  | 'chatbubble-ellipses-outline'
  | 'notifications-circle-outline'
  | 'logo-facebook'
  | 'logo-twitter'
  | 'logo-ionic'
  | 'timer-outline'
  | 'ellipse-outline'
  | 'close-circle-outline'
  | 'logo-wechat'
  | 'person-circle'
  | 'exit'
  | 'checkmark-circle-outline'
  | 'ellipse-outline'
  | 'bar-chart-outline'
  | 'server-outline'
  | 'share-social-outline'
  | 'newspaper-outline'
  | 'add-circle-outline'
  | 'remove-circle-outline';

export const IONIC_ICON: IIonicIcons = {
  help: 'help-circle-outline',
  settings: 'settings-outline',
  notifications: 'notifications-circle-outline',
  support: 'logo-wechat',
  facebook: 'logo-facebook',
  twitter: 'logo-twitter',
  menuItem: 'logo-ionic',
  expiration: 'timer-outline',
  close: 'close-circle-outline',
  profile: 'person-circle',
  exit: 'exit',
  selectedCollection: 'checkmark-circle-outline',
  notSelectedCollection: 'ellipse-outline',
  menuStatistics: 'bar-chart-outline',
  menuCollections: 'server-outline',
  menuSetup: 'share-social-outline',
  menuPublish: 'newspaper-outline',
  plus: 'add-circle-outline',
  minus: 'remove-circle-outline',
};

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  @Input() public icon: IICon;
  @Input() public size: string;

  public get ionIcon(): IIonIcon {
    return IONIC_ICON[this.icon];
  }
}
