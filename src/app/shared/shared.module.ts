import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { InputComponent } from './components/input/input.component';
import { IconComponent } from './components/icon/icon.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogoComponent } from './components/logo/logo.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { PaginationPipe } from './pipes/pagination.pipe';
import { ImageComponent } from './components/image/image.component';
import { MediaCardComponent } from './components/media-card/media-card.component';
import { MoreButtonComponent } from './components/more-button/more-button.component';
import { LineCardComponent } from './components/line-card/line-card.component';
import { SwitchComponent } from './components/switch/switch.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { SelectComponent } from './components/select/select.component';
import { IonicModule } from '@ionic/angular';
import { HelpComponent } from './components/help/help.component';
import { ImageModalComponent } from './components/image-modal/image-modal.component';
import { RadioComponent } from './components/radio/radio.component';
import { DateTimePickerComponent } from './components/date-time-picker/date-time-picker.component';
import { EmailModalComponent } from './components/email-modal/email-modal.component';
import { PlusMinusComponent } from './components/plus-minus-button/plus-minus-button.component';

const PIPES: any[] = [PaginationPipe];

const COMPONENTS: any[] = [
  ButtonComponent,
  InputComponent,
  IconComponent,
  LogoComponent,
  TextareaComponent,
  CheckboxComponent,
  MoreButtonComponent,
  ImageComponent,
  MediaCardComponent,
  LineCardComponent,
  TabsComponent,
  SwitchComponent,
  SelectComponent,
  HelpComponent,
  ImageModalComponent,
  RadioComponent,
  DateTimePickerComponent,
  EmailModalComponent,
  PlusMinusComponent,
];

@NgModule({
  declarations: [...COMPONENTS, ...PIPES],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, TextFieldModule, IonicModule],
  exports: [...COMPONENTS, ...PIPES],
})
export class SharedModule {}
