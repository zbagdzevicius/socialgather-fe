import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pagination',
  pure: true,
})
export class PaginationPipe implements PipeTransform {
  public transform<T>(values: T[], objectsPerPage: number = 10, page: number = 1): any {
    if (!values) {
      return;
    }

    const startingPage: number = objectsPerPage * page - objectsPerPage;
    const endingPage: number = startingPage + objectsPerPage;
    const currentPageObjects: T[] = values.slice(startingPage, endingPage);

    return currentPageObjects;
  }
}
