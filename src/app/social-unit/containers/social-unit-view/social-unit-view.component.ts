import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  SocialUnitEnabledSwitchAction,
  SocialUnitGetListAction,
  SocialUnitState,
  SocialUnitSwitchSocialNetworkTypeAction,
} from '../../../../core/store/social-unit.state';
import { ICollectionSocialUnit, ISocialUnitEnabledSwitch } from '../../../../core/models/social-unit.interface';
import { Observable } from 'rxjs';
import { SOCIAL_NETWORK_TYPE } from '../../../../core/models/social-network.interface';
import { IModalOptions } from '../../../../core/models/modal.interface';
import { HELP_KEY, HELP_TYPE } from '../../../../core/models/help.interface';

@Component({
  selector: 'app-social-unit-view',
  templateUrl: './social-unit-view.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocialUnitViewComponent implements OnInit {
  @Select(SocialUnitState.socialUnitList) public collectionSocialUnitList$: Observable<ICollectionSocialUnit[]>;
  @Select(SocialUnitState.socialUnitSocialNetworkType) public socialUnitSocialNetworkType$: Observable<SOCIAL_NETWORK_TYPE>;
  public helpOptions: IModalOptions = {
    key: HELP_KEY.SOCIAL_NETWORKS_UNITS,
    type: HELP_TYPE.LIST,
  };

  constructor(private store: Store) {}

  public ngOnInit(): void {
    this.store.dispatch(new SocialUnitGetListAction());
  }

  public onSocialNetworkTypeSwitch(socialNetworkType: SOCIAL_NETWORK_TYPE): void {
    this.store.dispatch(new SocialUnitSwitchSocialNetworkTypeAction(socialNetworkType));
  }

  public onSocialUnitEnabledSwitch(socialUnitEnabledSwitch: ISocialUnitEnabledSwitch): void {
    this.store.dispatch(new SocialUnitEnabledSwitchAction(socialUnitEnabledSwitch));
  }
}
