import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

import { SOCIAL_NETWORK_TYPE, socialNetworkProviderList } from '../../../../core/models/social-network.interface';
import { ICollectionSocialUnit, ISocialUnitEnabledSwitch } from '../../../../core/models/social-unit.interface';

@Component({
  selector: 'app-social-unit-list',
  templateUrl: './social-unit-list.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocialUnitListComponent {
  @Input() public collectionSocialUnitList: ICollectionSocialUnit[];
  @Input() public socialUnitSocialNetworkType: SOCIAL_NETWORK_TYPE;

  @Output() public socialUnitTypeSwitch: EventEmitter<SOCIAL_NETWORK_TYPE> = new EventEmitter<SOCIAL_NETWORK_TYPE>();
  @Output() public socialUnitEnabledSwitch: EventEmitter<ISocialUnitEnabledSwitch> = new EventEmitter<ISocialUnitEnabledSwitch>();

  public get socialUnitsSocialNetworksList(): SOCIAL_NETWORK_TYPE[] {
    return socialNetworkProviderList.filter((provider) => provider.unitEnabled).map((provider) => provider.socialNetworkType);
  }

  public onEnabledChange(enabled: boolean, socialUnitId: string): void {
    this.socialUnitEnabledSwitch.emit({
      enabled,
      socialUnitId,
    });
  }

  public onSocialNetworkSwitch(socialNetworkType: string): void {
    this.socialUnitTypeSwitch.emit(socialNetworkType as SOCIAL_NETWORK_TYPE);
  }
}
