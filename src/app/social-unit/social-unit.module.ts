import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialUnitViewComponent } from './containers/social-unit-view/social-unit-view.component';
import { SocialUnitListComponent } from './components/social-unit-list/social-unit-list.component';
import { RouterModule } from '@angular/router';
import { SOCIAL_UNIT_ROUTES } from './social-unit.routes';
import { SharedModule } from '../shared/shared.module';
import { NgxsModule } from '@ngxs/store';
import { SocialUnitState } from '../../core/store/social-unit.state';
import { SocialUnitService } from '../../core/services/social-unit/social-unit.service';

const EXPORTED_COMPONENTS: any[] = [SocialUnitViewComponent];

@NgModule({
  declarations: [...EXPORTED_COMPONENTS, SocialUnitListComponent],
  imports: [CommonModule, RouterModule.forChild(SOCIAL_UNIT_ROUTES), SharedModule, NgxsModule.forFeature([SocialUnitState])],
  exports: [...EXPORTED_COMPONENTS],
  providers: [SocialUnitService],
})
export class SocialUnitModule {}
