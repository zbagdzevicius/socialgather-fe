import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { ICron } from '../../../../core/models/cron.interface';

@Component({
  selector: 'app-upcoming-cron-list',
  templateUrl: './upcoming-cron-list.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpcomingCronListComponent {
  @Input() public upcomingCronList: ICron[];
}
