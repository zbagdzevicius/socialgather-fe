import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpcomingCronComponent } from './containers/upcoming-cron/upcoming-cron.component';
import { NgxsModule } from '@ngxs/store';
import { CronState } from '../../core/store/cron.state';
import { SharedModule } from '../shared/shared.module';
import { UpcomingCronListComponent } from './components/upcoming-cron-list/upcoming-cron-list.component';

const EXPORTED_COMPONENTS: any[] = [UpcomingCronComponent];

@NgModule({
  declarations: [...EXPORTED_COMPONENTS, UpcomingCronListComponent],
  imports: [CommonModule, NgxsModule.forFeature([CronState]), SharedModule],
  exports: EXPORTED_COMPONENTS,
})
export class UpcomingModule {}
