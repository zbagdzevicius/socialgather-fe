import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { CronGetUpcomingListAction, CronState } from '../../../../core/store/cron.state';
import { Observable } from 'rxjs';
import { ICron } from '../../../../core/models/cron.interface';

@Component({
  selector: 'app-upcoming-cron',
  templateUrl: './upcoming-cron.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpcomingCronComponent implements OnInit {
  @Select(CronState.upcomingCronList) public upcomingCronList$: Observable<ICron[]>;

  constructor(private store: Store) {}

  public ngOnInit(): void {
    this.store.dispatch(new CronGetUpcomingListAction());
  }
}
