import { Route, Routes } from '@angular/router';
import { HomeLayoutComponent } from './layout/layouts/home-layout/home-layout.component';
import { MainLayoutComponent } from './layout/layouts/main-layout/main-layout.component';
import { ActivateIfNotLoggedInGuard } from '../core/guards/not-auth.guard';
import { ActivateIfLoggedInGuard } from '../core/guards/auth.guard';
import { AppComponent } from './app.component';
import { CollectionGuard } from '../core/guards/collection.guard';

export const FEATURES_ROUTES: Routes = [
  {
    path: 'auth',
    canActivate: [ActivateIfNotLoggedInGuard],
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'statistics',
    canActivate: [ActivateIfLoggedInGuard, CollectionGuard],
    loadChildren: () => import('./statistics/statistics.module').then((m) => m.StatisticsModule),
  },
  {
    path: 'collection',
    canActivate: [ActivateIfLoggedInGuard],
    loadChildren: () => import('./collection/collection.module').then((m) => m.CollectionModule),
  },
  {
    path: 'cron',
    canActivate: [ActivateIfLoggedInGuard, CollectionGuard],
    loadChildren: () => import('./cron/cron.module').then((m) => m.CronModule),
  },
  {
    path: 'information',
    loadChildren: () => import('./information/information.module').then((m) => m.InformationModule),
  },
  {
    path: '',
    canActivate: [ActivateIfNotLoggedInGuard],
    component: AppComponent,
  },
];

interface IPageLayoutConfig {
  [key: string]: {
    routesToApplyTemplate: string[];
    component: any;
  };
}

const pagesLayoutsConfigs: IPageLayoutConfig = {
  mainLayoutPages: {
    routesToApplyTemplate: ['statistics', 'collection', 'cron'],
    component: MainLayoutComponent,
  },
  homeLayoutPages: {
    routesToApplyTemplate: ['auth', 'information', ''],
    component: HomeLayoutComponent,
  },
};

export function APP_ROUTES_WITH_LAYOUTS(routes: Routes): Routes {
  const appRoutesWithLayouts: Routes = [];

  routes.forEach((featureRoute) => {
    if (featureRoute.path !== '*' || featureRoute.redirectTo === undefined) {
      const pagesLayoutsConfigKey: string =
        Object.keys(pagesLayoutsConfigs).find((pageLayoutConfigKey) => {
          return pagesLayoutsConfigs[pageLayoutConfigKey].routesToApplyTemplate.includes(featureRoute.path);
        }) || 'mainLayoutPages';

      const routeWithTemplate: Route = {
        path: featureRoute.path,
        component: pagesLayoutsConfigs[pagesLayoutsConfigKey].component,
        children: [
          {
            ...featureRoute,
            path: '',
          },
        ],
      };
      appRoutesWithLayouts.push(routeWithTemplate);
    }
  });

  appRoutesWithLayouts.push({
    path: '**',
    redirectTo: 'information/not-found',
  });

  return appRoutesWithLayouts;
}
