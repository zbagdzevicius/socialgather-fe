import { AfterViewChecked, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { AuthState, LoginAction, LogoutAction } from '../core/store/auth.state';
import { tap } from 'rxjs/operators';
import { GoToCollectionSelectAction, GoToLoginAction } from '../core/store/route.state';
import { StateResetAll } from 'ngxs-reset-plugin';
import { GetSelfUserAction, UserState } from '../core/store/user.state';
import { Observable } from 'rxjs';
import { AppState } from 'src/core/store/app.state';
import { IUser } from 'src/core/models/user.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, AfterViewChecked {
  @Select(AppState.isLoading) public isLoading$: Observable<boolean>;
  @Select(UserState.user) public user$: Observable<IUser>;

  constructor(private _store: Store, private _actions$: Actions, private _cdr: ChangeDetectorRef) {}

  public ngOnInit(): void {
    if (!this._store.selectSnapshot(AuthState.isAuthenticated)) {
      this._store.dispatch(new GoToLoginAction());
    }
    this._handleLogout();
    this._handleLogin();
  }

  public ngAfterViewChecked(): void {
    this._cdr.detectChanges();
  }

  private _handleLogout(): void {
    this._actions$
      .pipe(
        ofActionSuccessful(LogoutAction),
        tap((_) => {
          this._store.dispatch(new StateResetAll());
          this._store.dispatch(new GoToLoginAction());
        }),
      )
      .subscribe();
  }

  private _handleLogin(): void {
    this._actions$
      .pipe(
        ofActionSuccessful(LoginAction),
        tap((_) => {
          if (this._store.selectSnapshot(AuthState.isAuthenticated)) {
            this._store.dispatch(new GetSelfUserAction());
            this._store.dispatch(new GoToCollectionSelectAction());
          } else {
            this._store.dispatch(new GoToLoginAction());
          }
        }),
      )
      .subscribe();
  }
}
