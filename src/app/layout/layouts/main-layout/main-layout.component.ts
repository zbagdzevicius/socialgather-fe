import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { IMenuItem } from '../../../../core/models/app.interface';
import { ICollection } from '../../../../core/models/collection.interface';
import { AppState } from '../../../../core/store/app.state';
import { LogoutAction } from '../../../../core/store/auth.state';
import { CollectionState } from '../../../../core/store/collection.state';

export interface ISidebarState {
  menu: boolean;
  profile: boolean;
  switchableSidebar: boolean;
}
export type ISideBar = 'menu' | 'profile';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class MainLayoutComponent implements OnInit {
  @Select(AppState.menu) public menu$: Observable<IMenuItem[]>;
  @Select(CollectionState.collection) public collection$: Observable<ICollection | null>;
  public activeMenuItemUrl: string;
  public sideBarsState: ISidebarState = {
    menu: true,
    profile: true,
    switchableSidebar: false,
  };

  constructor(private store: Store, private activatedRoute: ActivatedRoute, private router: Router) {}

  public ngOnInit(): void {
    this.activatedRoute.url.pipe(tap((urlSegmentList) => (this.activeMenuItemUrl = this.router.url))).subscribe();
  }

  public sideBarToggle(sidebar: ISideBar): void {
    if (this.sideBarsState.switchableSidebar) {
      this.sideBarsState[sidebar] = !this.sideBarsState[sidebar];
    }
  }

  public logout(): void {
    this.store.dispatch(new LogoutAction());
  }
}
