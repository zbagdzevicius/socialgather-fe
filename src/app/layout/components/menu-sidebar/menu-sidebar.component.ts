import { Component, EventEmitter, Input, Output } from '@angular/core';

import { IMenuItem } from '../../../../core/models/app.interface';
import { ICollection } from '../../../../core/models/collection.interface';

@Component({
  selector: 'app-menu-sidebar',
  templateUrl: './menu-sidebar.component.html',
})
export class MenuSidebarComponent {
  @Input() public menu: IMenuItem[];
  @Input() public collection: ICollection;
  @Input() public activeMenuItemUrl: string;

  @Output() public toggleClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() public logoutClick: EventEmitter<void> = new EventEmitter<void>();

  get disabled(): boolean {
    return !!this.collection;
  }
}
