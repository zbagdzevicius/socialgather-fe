import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';

import { IMenuItem } from '../../../../core/models/app.interface';

@Component({
  selector: 'app-menu-list-item',
  templateUrl: './menu-list-item.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuListItemComponent {
  @Input() public menuItem: IMenuItem;
  @Input() public active: boolean;
  @Input() public disabled: boolean;
  public activated: boolean;
}
