import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';

import { AuthState } from '../../core/store/auth.state';
import { CollectionState } from '../../core/store/collection.state';
import { RoutingState } from '../../core/store/route.state';
import { ProfileModule } from '../profile/profile.module';
import { SharedModule } from '../shared/shared.module';
import { UpcomingModule } from '../upcoming/upcoming.module';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';
import { MenuListItemComponent } from './components/menu-list-item/menu-list-item.component';
import { MenuMobileComponent } from './components/menu-mobile/menu-mobile.component';
import { MenuSidebarComponent } from './components/menu-sidebar/menu-sidebar.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';

const LAYOUTS: any[] = [HomeLayoutComponent, MainLayoutComponent];
const COMPONENTS: any[] = [MenuSidebarComponent, MenuMobileComponent];

@NgModule({
  declarations: [...LAYOUTS, ...COMPONENTS, MenuListItemComponent, LogoutButtonComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    NgxsModule.forFeature([RoutingState, AuthState, CollectionState]),
    UpcomingModule,
    ProfileModule,
  ],
  exports: [...COMPONENTS, ...LAYOUTS],
})
export class LayoutModule {}
