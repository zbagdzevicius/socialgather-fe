import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import {
  ISocialNetwork,
  SOCIAL_NETWORK_TYPE,
  socialNetworkProviderList,
  ISocialNetworkProvider,
} from '../../../../core/models/social-network.interface';

@Component({
  selector: 'app-social-network-list',
  templateUrl: './social-network-list.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocialNetworkListComponent {
  @Input() public socialNetworkList: ISocialNetwork[];
  @Output() public authorizeSocialNetwork: EventEmitter<SOCIAL_NETWORK_TYPE> = new EventEmitter<SOCIAL_NETWORK_TYPE>();

  public get addSocialNetworkProvidersList(): SOCIAL_NETWORK_TYPE[] {
    return socialNetworkProviderList.filter((provider) => provider.unitEnabled).map((provider) => provider.socialNetworkType);
  }

  public onSocialNetworkSelect(socialNetworkType: string): void {
    if (socialNetworkType) {
      this.authorizeSocialNetwork.emit(socialNetworkType as SOCIAL_NETWORK_TYPE);
    }
  }
}
