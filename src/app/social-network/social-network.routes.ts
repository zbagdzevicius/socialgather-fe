import { Routes } from '@angular/router';
import { SocialNetworkAuthorizeComponent } from './containers/social-network-authorize/social-network-authorize.component';

export const SOCIAL_NETWORK_ROUTES: Routes = [
  {
    path: 'authorize/:socialNetworkType/:userId',
    component: SocialNetworkAuthorizeComponent,
  },
];
