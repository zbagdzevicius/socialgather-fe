import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { SocialNetworkGetListAction, SocialNetworkState } from '../../../../core/store/social-network.state';
import { Observable } from 'rxjs';
import { ISocialNetwork, SOCIAL_NETWORK_TYPE } from '../../../../core/models/social-network.interface';
import { Navigate } from '@ngxs/router-plugin';
import { IModalOptions } from '../../../../core/models/modal.interface';
import { HELP_KEY, HELP_TYPE } from '../../../../core/models/help.interface';
import { AuthState } from 'src/core/store/auth.state';

@Component({
  selector: 'app-social-network-view',
  templateUrl: './social-network-view.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocialNetworkViewComponent implements OnInit {
  @Select(SocialNetworkState.socialNetworkList) public socialNetworkList$: Observable<ISocialNetwork[]>;
  public helpOptions: IModalOptions = {
    key: HELP_KEY.SOCIAL_NETWORKS,
    type: HELP_TYPE.LIST,
  };
  constructor(private _store: Store) {}

  public ngOnInit(): void {
    this._store.dispatch(new SocialNetworkGetListAction());
  }

  public authorizeSocialNetwork(socialNetworkType: SOCIAL_NETWORK_TYPE): void {
    const authToken: boolean = this._store.selectSnapshot(AuthState.isAuthenticated);
    this._store.dispatch(new Navigate([`/collection/authorize/${socialNetworkType}/${authToken}`]));
  }
}
