import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { SocialNetworkAddAction } from '../../../../core/store/auth.state';
import { SOCIAL_NETWORK_TYPE } from '../../../../core/models/social-network.interface';

@Component({
  selector: 'app-social-network-authorize',
  templateUrl: './social-network-authorize.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocialNetworkAuthorizeComponent implements OnInit {
  constructor(private _store: Store, private _route: ActivatedRoute) {}

  public ngOnInit(): void {
    const socialNetworkType: SOCIAL_NETWORK_TYPE = this._route.snapshot.paramMap.get('socialNetworkType') as SOCIAL_NETWORK_TYPE;
    this._store.dispatch(new SocialNetworkAddAction(socialNetworkType));
  }
}
