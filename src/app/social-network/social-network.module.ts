import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialNetworkViewComponent } from './containers/social-network-view/social-network-view.component';
import { RouterModule } from '@angular/router';
import { SOCIAL_NETWORK_ROUTES } from './social-network.routes';
import { SocialNetworkListComponent } from './components/social-network-list/social-network-list.component';
import { NgxsModule } from '@ngxs/store';
import { SocialNetworkState } from '../../core/store/social-network.state';
import { SharedModule } from '../shared/shared.module';
import { SocialNetworkAuthorizeComponent } from './containers/social-network-authorize/social-network-authorize.component';
import { UserState } from '../../core/store/user.state';

const EXPORTED_COMPONENTS: any[] = [SocialNetworkViewComponent, SocialNetworkAuthorizeComponent];

@NgModule({
  declarations: [...EXPORTED_COMPONENTS, SocialNetworkListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(SOCIAL_NETWORK_ROUTES),
    NgxsModule.forFeature([SocialNetworkState, UserState]),
    SharedModule,
  ],
  exports: [EXPORTED_COMPONENTS],
})
export class SocialNetworkModule {}
