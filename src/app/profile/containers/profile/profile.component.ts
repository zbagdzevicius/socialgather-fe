import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { Select } from '@ngxs/store';
import { UserState } from '../../../../core/store/user.state';
import { IUser } from '../../../../core/models/user.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent {
  @Select(UserState.user) public user$: Observable<IUser>;
}
