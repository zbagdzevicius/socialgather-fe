import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileCardComponent } from './components/profile-card/profile-card.component';
import { SharedModule } from '../shared/shared.module';
import { NgxsModule } from '@ngxs/store';
import { AppState } from '../../core/store/app.state';
import { ProfileComponent } from './containers/profile/profile.component';

const COMPONENTS: any[] = [ProfileComponent];

@NgModule({
  declarations: [...COMPONENTS, ProfileCardComponent],
  imports: [CommonModule, SharedModule, NgxsModule.forFeature([AppState])],
  exports: COMPONENTS,
})
export class ProfileModule {}
