import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IUser } from '../../../../core/models/user.interface';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileCardComponent {
  @Input() public user: IUser;
}
