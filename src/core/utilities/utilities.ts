export function createUrlQuery(paramsObject: any, prefix?: string, suffix?: string): string {
  if (!paramsObject) {
    return '';
  }
  const keys: string[] = Object.keys(paramsObject);
  let url: string = prefix ? `${prefix}?` : '?';

  keys.forEach((key, index) => {
    if (index === 0) {
      url = url.concat(key, '=', paramsObject[key]);
    } else {
      url = url.concat('&', key, '=', paramsObject[key]);
    }
  });

  if (suffix) {
    url = url.concat(suffix);
  }

  return url;
}

export function chunk<T>(arr: T[], size: number): T[][] {
  return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) => arr.slice(i * size, i * size + size));
}
