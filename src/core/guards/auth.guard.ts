import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';

import { AuthState } from '../store/auth.state';

@Injectable({
  providedIn: 'root',
})
export class ActivateIfLoggedInGuard implements CanActivate {
  constructor(private store: Store) {}

  public canActivate(): boolean {
    return this.store.selectSnapshot(AuthState.isAuthenticated);
  }
}
