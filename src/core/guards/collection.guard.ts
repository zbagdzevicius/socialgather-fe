import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { CollectionState } from '../store/collection.state';
import { ICollection } from '../models/collection.interface';

@Injectable({
  providedIn: 'root',
})
export class CollectionGuard implements CanActivate {
  constructor(private store: Store) {}

  public canActivate(): boolean {
    const collection: ICollection = this.store.selectSnapshot(CollectionState.collection);
    return !!collection;
  }
}
