import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthState } from '../store/auth.state';
import { GoToCollectionSelectAction } from '../store/route.state';

@Injectable({
  providedIn: 'root',
})
export class ActivateIfNotLoggedInGuard implements CanActivate {
  constructor(private store: Store) {}

  public canActivate(): boolean {
    const isLoggedIn: boolean = this.store.selectSnapshot(AuthState.isAuthenticated);
    if (isLoggedIn) {
      this.store.dispatch(new GoToCollectionSelectAction());
    }
    return !isLoggedIn;
  }
}
