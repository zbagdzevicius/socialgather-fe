import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IStatistics } from '../../models/statistics.interface';

function generateDays(start: Date, end: Date): Date[] {
  return [...new Array(30)].map((index) => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  });
}

@Injectable({
  providedIn: 'root',
})
export class StatisticsService {
  constructor(private httpClient: HttpClient) {}

  private static makeId(length: number = 3): number {
    let result: string = '';
    const characters: string = '123456789';
    const charactersLength: number = characters.length;
    for (let i: number = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return Number(result);
  }

  public get(): Observable<IStatistics> {
    const days: Date[] = generateDays(new Date('2010 05 19'), new Date());
    const statistics: IStatistics = {
      pagesEngagedUsers: [],
      pagesFansLocationList: [
        {
          fans: 36491,
          location: 'Lithuania',
        },
        {
          fans: 16982,
          location: 'Sweden',
        },
        {
          fans: 5913,
          location: 'Latvia',
        },
        {
          fans: 1394,
          location: 'Russia',
        },
        {
          fans: 16986,
          location: 'Others',
        },
      ],
      pagesImpressions: [],
      pagesReactions: [],
      pagesTotalActions: [],
    };

    days.forEach((date) => {
      statistics.pagesEngagedUsers.push({ count: 2 * StatisticsService.makeId(5), date });
      statistics.pagesImpressions.push({ count: 3 * StatisticsService.makeId(5), date });
      statistics.pagesReactions.push({ count: 4 * StatisticsService.makeId(5), date });
      statistics.pagesTotalActions.push({ count: 5 * StatisticsService.makeId(5), date });
    });
    return new Observable<IStatistics>((subscriber) => {
      subscriber.next(statistics);
    });
  }
}
