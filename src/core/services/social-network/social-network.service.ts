import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISocialNetwork } from '../../models/social-network.interface';

@Injectable({
  providedIn: 'root',
})
export class SocialNetworkService {
  constructor(private httpClient: HttpClient) {}

  public get(socialNetworkId: string): Observable<ISocialNetwork> {
    return this.httpClient.get<ISocialNetwork>(`social-network/${socialNetworkId}`);
  }

  public getList(): Observable<ISocialNetwork[]> {
    return this.httpClient.get<ISocialNetwork[]>('social-network/list');
  }
}
