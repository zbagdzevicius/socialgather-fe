import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IMessage } from 'src/core/models/app.interface';
import { ICroniclerEmail } from 'src/core/models/cronicler-email.interface';
import {
  DEFAULT_IMAGES_PER_PAGE,
  ICroniclerImage,
  ICroniclerImageSearch,
  IFileUploadResponse,
} from 'src/core/models/cronicler-image.interface';
import { createUrlQuery } from 'src/core/utilities/utilities';

@Injectable({
  providedIn: 'root',
})
export class ApiServiceService {
  constructor(private httpClient: HttpClient) {}

  public uploadImage(blob: Blob): Observable<IFileUploadResponse> {
    const formData: FormData = new FormData();
    formData.append('file', blob);
    return this.httpClient.post<IFileUploadResponse>('api/image-upload', formData);
  }

  public getImage(imageSearch: ICroniclerImageSearch): Observable<ICroniclerImage[]> {
    return this.httpClient.get<ICroniclerImage[]>(
      `api/images-api/image-search/${imageSearch.provider}/keyword/${encodeURI(imageSearch.query)}${createUrlQuery({
        perPage: imageSearch.perPage || DEFAULT_IMAGES_PER_PAGE,
      })}`,
    );
  }

  public sendEmail(email: ICroniclerEmail): Observable<IMessage> {
    return this.httpClient.post<IMessage>('api/email-api/send', email);
  }
}
