import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ICollection } from '../../models/collection.interface';

@Injectable({
  providedIn: 'root',
})
export class CollectionService {
  constructor(private httpClient: HttpClient) {}

  public create(collection: ICollection): Observable<ICollection> {
    return this.httpClient.post<ICollection>('collection', collection);
  }

  public getList(): Observable<ICollection[]> {
    return this.httpClient.get<ICollection[]>('collection/list');
  }
}
