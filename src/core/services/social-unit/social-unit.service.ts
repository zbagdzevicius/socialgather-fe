import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICollectionSocialUnit, ISocialNetworkTypeSocialUnitList, ISocialUnitEnabledSwitch } from '../../models/social-unit.interface';

@Injectable()
export class SocialUnitService {
  constructor(private httpClient: HttpClient) {}

  public getList(): Observable<ISocialNetworkTypeSocialUnitList[]> {
    return this.httpClient.get<ISocialNetworkTypeSocialUnitList[]>('social-unit/list');
  }

  public updateEnabledSwitch(socialUnitEnabledSwitch: ISocialUnitEnabledSwitch): Observable<ICollectionSocialUnit> {
    return this.httpClient.put<ICollectionSocialUnit>(`social-unit/${socialUnitEnabledSwitch.socialUnitId}`, socialUnitEnabledSwitch);
  }
}
