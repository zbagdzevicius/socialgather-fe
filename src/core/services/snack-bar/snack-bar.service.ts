import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {
  constructor(public toastController: ToastController) {}

  public async presentToast(message: string): Promise<void> {
    const toast: HTMLIonToastElement = await this.toastController.create({
      message,
      duration: 2000,
    });
    await toast.present();
  }
}
