import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICron, ICronListPayload, ICronQuery } from '../../models/cron.interface';
import { createUrlQuery } from '../../utilities/utilities';

@Injectable({
  providedIn: 'root',
})
export class CronService {
  constructor(private httpClient: HttpClient) {}

  public create(collection: ICron): Observable<ICron> {
    return this.httpClient.post<ICron>('cron', collection);
  }

  public list(cronQuery?: ICronQuery): Observable<ICronListPayload> {
    return this.httpClient.get<ICronListPayload>(`cron${createUrlQuery(cronQuery)}`);
  }
}
