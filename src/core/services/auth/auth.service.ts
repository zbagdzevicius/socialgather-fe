import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser, IUserLoginForm, IUserRegistrationForm } from '../../models/user.interface';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ILoginStatus } from '../../models/auth.interface';
import { environment } from '../../../environments/environment';
import { SOCIAL_NETWORK_TYPE } from '../../models/social-network.interface';
import { IMessage } from 'src/core/models/app.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  public login(loginForm: IUserLoginForm): Observable<ILoginStatus> {
    const body: HttpParams = new HttpParams().set('username', loginForm.username).set('password', loginForm.password);

    return this.httpClient.post<ILoginStatus>('auth/login', body);
  }

  public socialLogin(socialNetworkType: SOCIAL_NETWORK_TYPE): void {
    window.location.assign(`${environment.backEndUrl}/auth/${socialNetworkType}`);
  }

  public addSocialNetwork(socialNetworkType: SOCIAL_NETWORK_TYPE): void {
    window.location.assign(`${environment.backEndUrl}/auth/${socialNetworkType}/add-social-units`);
  }

  public socialLoginVerify(accessToken: string): Observable<IMessage> {
    return this.httpClient.get<IMessage>(`auth/social-login-verify/${accessToken}`);
  }

  public registration(registrationForm: IUserRegistrationForm): Observable<IUser> {
    const body: HttpParams = new HttpParams()
      .set('username', registrationForm.username)
      .set('password', registrationForm.password)
      .set('passwordRepeat', registrationForm.password);

    return this.httpClient.post<IUser>('auth/registration', body);
  }

  public logout(): Observable<any> {
    return this.httpClient.post('auth/logout', {});
  }
}
