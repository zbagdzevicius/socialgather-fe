import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from './modal.component';
import { IEmailModalOptions, IImageSelectModalOptions, IModalOptions } from '../../models/modal.interface';
import { ImageModalComponent } from 'src/app/shared/components/image-modal/image-modal.component';
import { EmailModalComponent } from 'src/app/shared/components/email-modal/email-modal.component';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  constructor(public modalController: ModalController) {}

  public async openModal(modalOptions: IModalOptions): Promise<HTMLIonModalElement> {
    const modal: HTMLIonModalElement = await this.modalController.create({
      component: ModalComponent,
      componentProps: { modalOptions },
      mode: 'ios',
      animated: false,
    });
    await modal.present();
    return modal;
  }

  public async openImageSelectModal(imageSelectModalOptions: IImageSelectModalOptions): Promise<HTMLIonModalElement> {
    const modal: HTMLIonModalElement = await this.modalController.create({
      component: ImageModalComponent,
      componentProps: { imageSelectModalOptions },
      mode: 'ios',
      animated: false,
      cssClass: 'image-modal-container',
    });
    await modal.present();
    return modal;
  }

  public async openEmailModal(emailModalOptions: IEmailModalOptions): Promise<HTMLIonModalElement> {
    const modal: HTMLIonModalElement = await this.modalController.create({
      component: EmailModalComponent,
      componentProps: { emailModalOptions },
      mode: 'ios',
      animated: false,
      cssClass: 'email-modal-container',
    });
    await modal.present();
    return modal;
  }
}
