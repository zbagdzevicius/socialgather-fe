import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input } from '@angular/core';
import { IModalOptions } from '../../models/modal.interface';
import { IHelpInformation } from '../../models/help.interface';
import { ModalController } from '@ionic/angular';
import { HELP_MODAL_OPTIONS } from '../../constants/help.constants';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalComponent {
  @Input() public modalOptions: IModalOptions;
  public viewIsReadyForIframe: boolean = false;

  constructor(public modalController: ModalController, public domSanitizer: DomSanitizer) {}

  public get helpInformation(): IHelpInformation {
    const helpInformation: IHelpInformation = HELP_MODAL_OPTIONS[this.modalOptions.key][this.modalOptions.type];
    return {
      ...helpInformation,
      iframeSrc: helpInformation.iframeSrc
        ? this.domSanitizer.bypassSecurityTrustResourceUrl(helpInformation.iframeSrc as string)
        : undefined,
    };
  }

  public async onCloseClick(): Promise<boolean> {
    return await this.modalController.dismiss({
      dismissed: true,
    });
  }
}
