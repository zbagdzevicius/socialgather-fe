import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LogoutAction } from '../store/auth.state';
import { CollectionState } from '../store/collection.state';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store) {}

  public intercept<T>(req: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
    const collectionId: string = this.store.selectSnapshot(CollectionState.collection)?._id;
    const authenticatedRequest: HttpRequest<T> = req.clone({
      headers: req.headers.set('collectionId', collectionId),
      // This is needed in order to pass cookies to the server. Without this cookies won't be served
      withCredentials: true,
    });

    return next.handle(authenticatedRequest).pipe(
      tap(
        () => {},
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.store.dispatch(new LogoutAction());
            }
          }
        },
      ),
    );
  }
}
