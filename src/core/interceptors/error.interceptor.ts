import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { SnackBarOpenAction } from '../store/app.state';
import { GoToLoginAction } from '../store/route.state';
import { StateResetAll } from 'ngxs-reset-plugin';
import { StatusCodes } from 'http-status-codes';
import { IMessage } from '../models/app.interface';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private _store: Store) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap((httpEvent) => {
        if (httpEvent.type === HttpEventType.Response) {
          const response: HttpResponse<IMessage> = httpEvent;
          if (response.body?.message) {
            this._store.dispatch(new SnackBarOpenAction(response.body.message));
          }
        }
      }),
      retry(1),
      catchError((error: HttpErrorResponse) => {
        if ([StatusCodes.UNAUTHORIZED, 0].includes(error.status)) {
          this._store.dispatch(new GoToLoginAction());
          this._store.dispatch(new StateResetAll());
        }
        const errorMessage: string = error.error instanceof ErrorEvent ? error.error.message : `${error.message}`;
        this._store.dispatch(new SnackBarOpenAction(errorMessage));
        return throwError(errorMessage);
      }),
    );
  }
}
