import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { Store } from '@ngxs/store';
import { LoadingStatusChangesAction } from '../store/app.state';

@Injectable()
export class APIInterceptor implements HttpInterceptor {
  private _requests: HttpRequest<any>[] = [];

  constructor(private _store: Store) {}

  public get someRequestsStillLoading(): boolean {
    return this._requests.length > 0;
  }

  public intercept<T>(req: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
    req.headers.set('Access-Control-Allow-Origin', '*');
    const url: string = req.url.startsWith('http') ? req.url : `${environment.backEndUrl}/${req.url}`;
    const apiReq: HttpRequest<T> = req.clone({ url });

    this._store.dispatch(new LoadingStatusChangesAction(true));

    return new Observable<HttpEvent<T>>((observer) => {
      const subscription: Subscription = next.handle(apiReq).subscribe(
        (event) => {
          if (event instanceof HttpResponse) {
            this._removeRequest(req);
            observer.next(event);
          }
        },
        (err) => {
          this._removeRequest(req);
          observer.error(err);
        },
        () => {
          this._removeRequest(req);
          observer.complete();
        },
      );

      return () => {
        this._removeRequest(req);
        subscription.unsubscribe();
      };
    });
  }

  private _removeRequest(req: HttpRequest<any>): void {
    const i: number = this._requests.indexOf(req);
    if (i >= 0) {
      this._requests.splice(i, 1);
    }

    this._store.dispatch(new LoadingStatusChangesAction(this.someRequestsStillLoading));
  }
}
