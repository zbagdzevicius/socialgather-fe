import { ErrorHandler, Injectable } from '@angular/core';

import * as Sentry from '@sentry/browser';

Sentry.init({
  dsn: 'https://47609d2b3abb40bf88d5e5aa3c7bfebf@o235509.ingest.sentry.io/5236637',
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  public handleError(error: any): void {
    throw error;
    const eventId: string = Sentry.captureException(error.originalError || error);
    // Sentry.showReportDialog({ eventId });
  }
}
