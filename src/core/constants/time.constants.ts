export const MAX_SCHEDULING_DATE: Date = new Date(new Date().getTime() + 60 * 60000 * 24 * 30);
export const MIN_SCHEDULING_TIME_FROM_CURRENT_DATE: number = 900000;
export const ONE_MINUTE_IN_MILLISECONDS: number = 60000;

export function getMinDate(): Date {
  return new Date(new Date().getTime() + MIN_SCHEDULING_TIME_FROM_CURRENT_DATE);
}
