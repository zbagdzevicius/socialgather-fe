import { IHelp, IHelpInformation } from '../models/help.interface';

const COLLECTION_CREATE_HELP_INFORMATION: IHelpInformation = {
  title: 'Collection Create',
  description: "Create new collection that will be visible in collections list. Social units won't be assigned to new collection.",
};
const COLLECTION_SELECT_HELP_INFORMATION: IHelpInformation = {
  title: 'Collection Select',
  description:
    'Select collection in order to be able to access publishing, social networks management, statistics functionality. Social Units number is count of activated social network pages/profiles.',
};
const SOCIAL_NETWORK_LIST_HELP_INFORMATION: IHelpInformation = {
  title: 'Social Networks List',
  description:
    'Social Networks list is used for adding social networks to current profile. Social networks are accessible from across application in all social networks collections (system-wide for current profile).',
};
const SOCIAL_NETWORKS_UNITS_LIST_HELP_INFORMATION: IHelpInformation = {
  title: 'Social Networks Units List',
  description:
    'Social networks units can be activated or deactivated for each social networks units collection. Social unit - social network account profile, page or group.',
};
const PUBLISH_CREATE_HELP_INFORMATION: IHelpInformation = {
  title: 'Schedule Post',
  description:
    'Post will be scheduled into all current selected collection social units. All fields must be filled in order to schedule a post. Image search input field is not required but should be filled in order to search for image. Only future date and time can be selected.',
};
const PUBLISH_LIST_HELP_INFORMATION: IHelpInformation = {
  title: 'Scheduled posts',
  description: 'Scheduled posts of current collection social units. Past posts are posts that already have been published',
};
const STATISTICS_LIST_HELP_INFORMATION: IHelpInformation = {
  title: 'Statistics',
  description: 'Statistics for social units of current selected collection.',
};

const SUPPORT_HELP_INFORMATION: IHelpInformation = {
  title: 'Support Chat',
  description: 'Right here you can ask any questions',
  iframeSrc: 'https://socialworks.herokuapp.com/chatbot',
};

export const HELP_MODAL_OPTIONS: IHelp = {
  collection: {
    create: COLLECTION_CREATE_HELP_INFORMATION,
    select: COLLECTION_SELECT_HELP_INFORMATION,
  },
  socialNetworks: {
    list: SOCIAL_NETWORK_LIST_HELP_INFORMATION,
  },
  socialNetworksUnits: {
    list: SOCIAL_NETWORKS_UNITS_LIST_HELP_INFORMATION,
  },
  publish: {
    create: PUBLISH_CREATE_HELP_INFORMATION,
    list: PUBLISH_LIST_HELP_INFORMATION,
  },
  statistics: {
    list: STATISTICS_LIST_HELP_INFORMATION,
  },
  support: {
    list: SUPPORT_HELP_INFORMATION,
  },
};
