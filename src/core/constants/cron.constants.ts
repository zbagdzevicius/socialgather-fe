export const MAX_CONTENT_LENGTH: number = 1000;
export const VALID_URL_REGEXP: string = '(\b(https?|ftp|file)://)?[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]';
