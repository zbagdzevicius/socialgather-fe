export interface IUserLoginForm {
  username: string;
  password: string;
  rememberMe?: boolean;
}

export interface IUserRegistrationForm extends IUserLoginForm {
  passwordRepeat: string;
}

export interface IUser {
  _id: string;
  username: string;
  picture: string;
}
