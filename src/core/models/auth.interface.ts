import { IMessage } from './app.interface';

export interface IAuth {
  authenticated: boolean;
}

export type IRegistrationStatus = IMessage;
export type ILoginStatus = IMessage;
