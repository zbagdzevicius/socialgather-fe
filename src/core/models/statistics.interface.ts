export interface IFansLocation {
  location: string;
  fans: number;
}

export interface IMetricsCount {
  date: Date;
  count: number;
}

export interface IStatistics {
  pagesImpressions: IMetricsCount[];
  pagesEngagedUsers: IMetricsCount[];
  pagesReactions: IMetricsCount[];
  pagesTotalActions: IMetricsCount[];
  pagesFansLocationList: IFansLocation[];
}

export enum STATISTICS_CATEGORY {
  ACTIONS = 'actions',
  REACTIONS = 'reactions',
  IMPRESSIONS = 'impressions',
  ENGAGED_USERS = 'engaged users',
  LOCATION = 'location',
}

export const statisticsCategories: STATISTICS_CATEGORY[] = [
  STATISTICS_CATEGORY.ACTIONS,
  STATISTICS_CATEGORY.REACTIONS,
  STATISTICS_CATEGORY.IMPRESSIONS,
  STATISTICS_CATEGORY.ENGAGED_USERS,
  STATISTICS_CATEGORY.LOCATION,
];
