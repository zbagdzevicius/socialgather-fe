export interface ICroniclerEmail {
  sender: string;
  subject: string;
  message: string;
}
