export interface IPaginationOptions {
  perPage: number;
  page: number;
}

export interface IListQuery {
  offset?: number;
  limit?: number;
}
