import { IListQuery } from './pagination.interface';

export interface ICron {
  content?: string;
  imageUrl?: string;
  linkUrl?: string;
  postOn: Date;
}

export interface IListCount {
  count: number;
}

export interface ICronListPayload extends IListCount {
  cronList: ICron[];
}

export enum CRON_LIST_TYPE {
  UPCOMING = 'upcoming',
  PAST = 'past',
}

export enum CRON_POST_TYPE {
  IMAGE = 'image',
  LINK = 'link',
  MESSAGE = 'message',
}

export const cronPostTypes: CRON_POST_TYPE[] = [CRON_POST_TYPE.IMAGE, CRON_POST_TYPE.LINK, CRON_POST_TYPE.MESSAGE];

export const cronListTypes: CRON_LIST_TYPE[] = [CRON_LIST_TYPE.UPCOMING, CRON_LIST_TYPE.PAST];

export interface ICronQuery extends IListQuery {
  cronListType?: CRON_LIST_TYPE;
}
