import { SafeResourceUrl } from '@angular/platform-browser';

export interface IHelpInformation {
  title?: string;
  description?: string;
  iframeSrc?: string | SafeResourceUrl;
}

export enum HELP_KEY {
  COLLECTION = 'collection',
  SOCIAL_NETWORKS = 'socialNetworks',
  SOCIAL_NETWORKS_UNITS = 'socialNetworksUnits',
  PUBLISH = 'publish',
  STATISTICS = 'statistics',
  SUPPORT = 'support',
}

export enum HELP_TYPE {
  CREATE = 'create',
  SELECT = 'select',
  LIST = 'list',
}

export interface IHelp {
  collection: {
    create: IHelpInformation;
    select: IHelpInformation;
  };
  socialNetworks: {
    list: IHelpInformation;
  };
  socialNetworksUnits: {
    list: IHelpInformation;
  };
  publish: {
    create: IHelpInformation;
    list: IHelpInformation;
  };
  statistics: {
    list: IHelpInformation;
  };
  support: {
    list: IHelpInformation;
  };
}
