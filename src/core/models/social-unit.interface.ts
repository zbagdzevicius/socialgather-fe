import { SOCIAL_NETWORK_TYPE } from './social-network.interface';

export enum SOCIAL_UNIT_TYPE {
  PAGE = 'page',
  PROFILE = 'profile',
  GROUP = 'group',
}

export interface ISocialUnit {
  name: string;
  type: SOCIAL_UNIT_TYPE;
  socialNetworkId: string;
  socialNetworkType: SOCIAL_NETWORK_TYPE;
}

export interface ICollectionSocialUnit {
  _id: any;
  name: string;
  type: SOCIAL_UNIT_TYPE;
  enabled: boolean;
}

export interface ISocialNetworkTypeSocialUnitList {
  socialNetworkType: SOCIAL_NETWORK_TYPE;
  socialUnitList: ICollectionSocialUnit[];
}

export interface ISocialUnitEnabledSwitch {
  enabled: boolean;
  socialUnitId: string;
}
