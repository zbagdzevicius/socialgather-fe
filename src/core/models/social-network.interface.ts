export enum SOCIAL_NETWORK_TYPE {
  FACEBOOK = 'facebook',
  TWITTER = 'twitter',
  REDDIT = 'reddit',
}

export interface ISocialNetwork {
  _id: string;
  name: string;
  type: SOCIAL_NETWORK_TYPE;
  userId: string;
  picture: string;
  expireOn: string;
  accessToken: string;
  refreshToken: string;
}

export interface ISocialNetworkProvider {
  socialNetworkType: SOCIAL_NETWORK_TYPE;
  loginEnabled: boolean;
  unitEnabled: boolean;
}

export const socialNetworkProviderList: ISocialNetworkProvider[] = [
  { socialNetworkType: SOCIAL_NETWORK_TYPE.FACEBOOK, loginEnabled: true, unitEnabled: true },
  { socialNetworkType: SOCIAL_NETWORK_TYPE.TWITTER, loginEnabled: true, unitEnabled: false },
  { socialNetworkType: SOCIAL_NETWORK_TYPE.REDDIT, loginEnabled: false, unitEnabled: true },
];
