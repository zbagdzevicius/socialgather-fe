import { ICroniclerEmail } from './cronicler-email.interface';
import { ICroniclerImage } from './cronicler-image.interface';
import { HELP_KEY, HELP_TYPE } from './help.interface';

export interface IModalOptions {
  type: HELP_TYPE;
  key: HELP_KEY;
}
export interface IImageSelectModalOptions {
  images: ICroniclerImage[];
}

export interface IEmailModalOptions {
  emailForm: ICroniclerEmail;
  valid?: boolean;
}
