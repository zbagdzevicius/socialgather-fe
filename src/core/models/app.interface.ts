import { IICon } from 'src/app/shared/components/icon/icon.component';

export interface IMenuItem {
  displayName: string;
  link: string;
  icon?: IICon;
}

export interface IMessage {
  message: string;
  success: boolean;
}
