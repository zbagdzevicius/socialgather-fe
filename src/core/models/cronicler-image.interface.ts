export interface ICroniclerImage {
  fullImageUrl: string;
  thumbnailImageUrl: string;
  query: string;
}

export enum IMAGE_PROVIDERS {
  UNSPLASH = 'unsplash',
  PEXELS = 'pexels',
  IMAGE_UPLOAD = 'upload image',
  IMAGE_URL = 'image url',
}

export const DEFAULT_IMAGES_PER_PAGE: number = 16;

export enum MAXIMUM_ITEMS_PER_PAGE {
  UNSPLASH = 30,
  PEXELS = 80,
}

export interface ICroniclerImageSearch {
  query: string;
  provider: IMAGE_PROVIDERS;
  perPage?: number;
}

export interface ImageSelect {
  selectedImage: ICroniclerImage;
}

export interface IImageToUpload {
  blob: Blob;
  imageUrl: string;
}

export interface IFileUploadResponse {
  imageUrl: string;
  success: boolean;
}
