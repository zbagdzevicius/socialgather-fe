import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ISocialNetwork } from '../models/social-network.interface';
import { SocialNetworkService } from '../services/social-network/social-network.service';
import { Observable } from 'rxjs';

export class SocialNetworkGetAction {
  public static readonly type: string = '[SocialNetwork] SocialNetworkGetAction';

  constructor(public socialNetworkId: string) {}
}

export class SocialNetworkGetListAction {
  public static readonly type: string = '[SocialNetwork] SocialNetworkGetListAction';
}

export interface SocialNetworkModel {
  socialNetworkList: ISocialNetwork[];
}

@Injectable()
@State<SocialNetworkModel>({
  name: 'socialNetwork',
  defaults: {
    socialNetworkList: [],
  },
})
export class SocialNetworkState {
  constructor(private _socialNetworkService: SocialNetworkService) {}

  @Selector()
  public static socialNetworkList(state: SocialNetworkModel): ISocialNetwork[] {
    return state.socialNetworkList;
  }

  @Action(SocialNetworkGetAction)
  public get(ctx: StateContext<SocialNetworkModel>, action: SocialNetworkGetAction): Observable<ISocialNetwork> {
    const socialNetworkList: ISocialNetwork[] = [...ctx.getState().socialNetworkList];
    return this._socialNetworkService.get(action.socialNetworkId).pipe(
      tap((socialNetwork) => {
        const socialNetworkToUpdateIndex: number = socialNetworkList.findIndex(
          (userSocialNetwork) => userSocialNetwork._id === action.socialNetworkId,
        );
        socialNetworkList[socialNetworkToUpdateIndex] = socialNetwork;
        ctx.patchState({ socialNetworkList });
      }),
    );
  }

  @Action(SocialNetworkGetListAction)
  public getList(ctx: StateContext<SocialNetworkModel>): Observable<ISocialNetwork[]> {
    return this._socialNetworkService.getList().pipe(
      tap((socialNetworkList) => {
        ctx.patchState({ socialNetworkList });
      }),
    );
  }
}
