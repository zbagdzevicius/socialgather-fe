import { Action, State, StateContext } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export class GoToHomeAction {
  public static readonly type: string = '[route] GoToHomeAction';
}

export class GoToLoginAction {
  public static readonly type: string = '[route] LoginAction';
}

export class GoToRegistrationAction {
  public static readonly type: string = '[route] GoToRegistrationAction';
}

export class GoToDashboardAction {
  public static readonly type: string = '[route] GoToDashboardAction';
}

export class GoToCollectionSelectAction {
  public static readonly type: string = '[route] GoToCollectionSelect';
}

export class GoToCollectionCreateAction {
  public static readonly type: string = '[route] GoToCollectionCreateAction';
}

export class GoToPrivacyPolicy {
  public static readonly type: string = '[route] GoToPrivacyPolicy';
}

@Injectable()
@State({
  name: 'route',
})
export class RoutingState {
  @Action(GoToHomeAction)
  public goToHome(ctx: StateContext<any>): Observable<void> {
    return ctx.dispatch(new Navigate(['/']));
  }

  @Action(GoToLoginAction)
  public goToLogin(ctx: StateContext<any>): Observable<void> {
    return ctx.dispatch(new Navigate(['/auth/login']));
  }

  @Action(GoToRegistrationAction)
  public goToRegistration(ctx: StateContext<any>): Observable<void> {
    return ctx.dispatch(new Navigate(['/auth/registration']));
  }

  @Action(GoToDashboardAction)
  public goToDashboard(ctx: StateContext<any>): Observable<void> {
    return ctx.dispatch(new Navigate(['/statistics']));
  }

  @Action(GoToCollectionSelectAction)
  public goToCollectionSelect(ctx: StateContext<any>): Observable<void> {
    return ctx.dispatch(new Navigate(['/collection']));
  }

  @Action(GoToCollectionCreateAction)
  public goToCollectionCreate(ctx: StateContext<any>): Observable<void> {
    return ctx.dispatch(new Navigate(['/collection/create']));
  }

  @Action(GoToPrivacyPolicy)
  public goToPrivacyPolicy(ctx: StateContext<any>): Observable<void> {
    return ctx.dispatch(new Navigate(['/information/privacy-policy']));
  }
}
