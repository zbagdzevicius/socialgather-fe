import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { CRON_LIST_TYPE, CRON_POST_TYPE, ICron, ICronListPayload, ICronQuery } from '../models/cron.interface';
import { CronService } from '../services/cron/cron.service';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

export class CronGetListAction {
  public static readonly type: string = '[Cron] CronGetListAction';

  constructor(public cronQuery?: ICronQuery) {}
}

export class CronGetUpcomingListAction {
  public static readonly type: string = '[Cron] CronGetUpcomingListAction';
}

export class SetCronPostTypeAction {
  public static readonly type: string = '[Cron] SetCronPostTypeAction';

  constructor(public cronPostType: CRON_POST_TYPE) {}
}

export class CronSwitchListType {
  public static readonly type: string = '[Cron] CronSwitchListType';

  constructor(public cronListType: CRON_LIST_TYPE) {}
}

export class CronPostAction {
  public static readonly type: string = '[Cron] CronPostAction';

  constructor(public cron: ICron) {}
}

export interface CronModel {
  cronPostType: CRON_POST_TYPE;
  cronListPayload: ICronListPayload;
  upcomingCronList: ICron[];
  cronQuery: ICronQuery;
  upcomingCronQuery: ICronQuery;
}

@Injectable()
@State<CronModel>({
  name: 'cron',
  defaults: {
    cronPostType: CRON_POST_TYPE.IMAGE,
    cronListPayload: {
      count: 0,
      cronList: [],
    },
    upcomingCronList: [],
    cronQuery: {
      cronListType: CRON_LIST_TYPE.UPCOMING,
      offset: 0,
      limit: 9,
    },
    upcomingCronQuery: {
      cronListType: CRON_LIST_TYPE.UPCOMING,
      offset: 0,
      limit: 10,
    },
  },
})
export class CronState {
  constructor(private cronService: CronService) {}

  @Selector()
  public static cronListPayload(state: CronModel): ICronListPayload {
    return state.cronListPayload;
  }

  @Selector()
  public static upcomingCronList(state: CronModel): ICron[] {
    return state.upcomingCronList;
  }

  @Selector()
  public static cronListType(state: CronModel): CRON_LIST_TYPE {
    return state.cronQuery.cronListType;
  }

  @Selector()
  public static cronPostType(state: CronModel): CRON_POST_TYPE {
    return state.cronPostType;
  }

  @Action(CronSwitchListType)
  public switchListType(ctx: StateContext<CronModel>, action: CronSwitchListType): void {
    const currentCronQuery: ICronQuery = ctx.getState().cronQuery;
    ctx.patchState({
      cronQuery: {
        ...currentCronQuery,
        cronListType: action.cronListType,
      },
    });
    ctx.dispatch(new CronGetListAction());
  }

  @Action(SetCronPostTypeAction)
  public setCronPostType(ctx: StateContext<CronModel>, action: SetCronPostTypeAction): void {
    ctx.patchState({ cronPostType: action.cronPostType });
  }

  @Action(CronGetListAction)
  public getCronList(ctx: StateContext<CronModel>, action: CronGetListAction): Observable<ICronListPayload> {
    let cronQuery: ICronQuery = ctx.getState().cronQuery;
    if (action.cronQuery) {
      cronQuery = {
        ...cronQuery,
        ...action.cronQuery,
      };
    }
    return this.cronService.list(cronQuery).pipe(
      tap((cronListPayload) => {
        ctx.patchState({
          cronListPayload,
        });
      }),
    );
  }

  @Action(CronGetUpcomingListAction)
  public getUpcomingCronList(ctx: StateContext<CronModel>): Observable<ICronListPayload> {
    return this.cronService.list(ctx.getState().upcomingCronQuery).pipe(
      tap((cronListRequest) => {
        ctx.patchState({
          upcomingCronList: cronListRequest.cronList,
        });
      }),
    );
  }

  @Action(CronPostAction)
  public postCron(ctx: StateContext<CronModel>, action: CronPostAction): Observable<ICron> {
    const cronPostType: CRON_POST_TYPE = ctx.getState().cronPostType;
    return this.cronService.create(this.cronWithFieldsByType(action.cron, cronPostType));
  }

  public cronWithFieldsByType(cron: ICron, type: CRON_POST_TYPE): ICron {
    if (type === CRON_POST_TYPE.IMAGE) {
      return {
        imageUrl: cron.imageUrl,
        content: cron.content,
        postOn: cron.postOn,
      };
    }
    if (type === CRON_POST_TYPE.LINK) {
      return {
        imageUrl: cron.linkUrl,
        content: cron.content,
        postOn: cron.postOn,
      };
    }
    // CRON_POST_TYPE.MESSAGE
    return {
      content: cron.content,
      postOn: cron.postOn,
    };
  }
}
