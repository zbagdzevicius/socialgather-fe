import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { ICollection } from '../models/collection.interface';
import { CollectionService } from '../services/collection/collection.service';
import { tap } from 'rxjs/operators';
import { SnackBarOpenAction } from './app.state';
import { GoToCollectionSelectAction, GoToDashboardAction } from './route.state';
import { Observable } from 'rxjs';

export class CollectionSwitchAction {
  public static readonly type: string = '[Collection] CollectionSwitchAction';

  constructor(public collection: ICollection) {}
}

export class CollectionCreateAction {
  public static readonly type: string = '[Collection] CollectionCreateAction';

  constructor(public collection: ICollection) {}
}

export class CollectionGetListAction {
  public static readonly type: string = '[Collection] CollectionGetListAction';
}

export interface CollectionStateModel {
  collection: ICollection;
  collectionList: ICollection[];
}

@Injectable()
@State<CollectionStateModel>({
  name: 'collection',
  defaults: {
    collection: null,
    collectionList: [],
  },
})
export class CollectionState {
  constructor(private _collectionService: CollectionService) {}

  @Selector()
  public static collection(state: CollectionStateModel): ICollection | null {
    return state.collection;
  }

  @Selector()
  public static collectionList(state: CollectionStateModel): ICollection[] {
    return state.collectionList;
  }

  @Action(CollectionSwitchAction)
  public switchCollection(ctx: StateContext<CollectionStateModel>, action: CollectionCreateAction): CollectionStateModel {
    return ctx.patchState({ collection: action.collection });
  }

  @Action(CollectionCreateAction)
  public create(ctx: StateContext<CollectionStateModel>, action: CollectionCreateAction): Observable<ICollection> {
    return this._collectionService.create(action.collection).pipe(
      tap((collection) => {
        ctx.dispatch(new GoToCollectionSelectAction());
        ctx.dispatch(new SnackBarOpenAction(`Collection: ${collection.name} created successfully`));
      }),
    );
  }

  @Action(CollectionGetListAction)
  public getList(ctx: StateContext<CollectionStateModel>): Observable<ICollection[]> {
    return this._collectionService.getList().pipe(
      tap((collectionList) => {
        ctx.patchState({ collectionList });
      }),
    );
  }
}
