import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { SocialUnitService } from '../services/social-unit/social-unit.service';
import { ICollectionSocialUnit, ISocialNetworkTypeSocialUnitList, ISocialUnitEnabledSwitch } from '../models/social-unit.interface';
import { SOCIAL_NETWORK_TYPE } from '../models/social-network.interface';
import { ImmutableContext } from '@ngxs-labs/immer-adapter';
import { Observable } from 'rxjs';

export class SocialUnitGetListAction {
  public static readonly type: string = '[SocialUnit] SocialUnitGetListAction';
}

export class SocialUnitSwitchSocialNetworkTypeAction {
  public static readonly type: string = '[SocialUnit] SocialUnitSwitchSocialNetworkTypeAction';

  constructor(public socialNetworkType: SOCIAL_NETWORK_TYPE) {}
}

export class SocialUnitEnabledSwitchAction {
  public static readonly type: string = '[SocialUnit] SocialUnitEnabledSwitchAction';

  constructor(public socialUnitEnabledSwitch: ISocialUnitEnabledSwitch) {}
}

export interface SocialUnitModel {
  socialNetworkTypeSocialUnitListArray: ISocialNetworkTypeSocialUnitList[];
  currentSocialNetworkType: SOCIAL_NETWORK_TYPE;
}

@Injectable()
@State<SocialUnitModel>({
  name: 'socialUnit',
  defaults: {
    socialNetworkTypeSocialUnitListArray: [],
    currentSocialNetworkType: null,
  },
})
export class SocialUnitState {
  constructor(private _socialUnitService: SocialUnitService) {}

  @Selector()
  public static socialUnitList(state: SocialUnitModel): ICollectionSocialUnit[] {
    if (state?.socialNetworkTypeSocialUnitListArray?.length) {
      return SocialUnitState.getSocialNetworkTypeSocialUnitList(state.socialNetworkTypeSocialUnitListArray, state.currentSocialNetworkType);
    } else {
      return [];
    }
  }

  @Selector()
  public static socialUnitSocialNetworkType(state: SocialUnitModel): SOCIAL_NETWORK_TYPE {
    return state.currentSocialNetworkType;
  }

  public static getSocialNetworkTypeSocialUnitList(
    socialNetworkTypeSocialUnitListArray: ISocialNetworkTypeSocialUnitList[],
    socialNetworkType: SOCIAL_NETWORK_TYPE,
  ): ICollectionSocialUnit[] {
    let collectionSocialUnit: ICollectionSocialUnit[] = [];

    socialNetworkTypeSocialUnitListArray.forEach((socialNetworkTypeSocialUnitList) => {
      if (socialNetworkTypeSocialUnitList.socialNetworkType === socialNetworkType || !socialNetworkType) {
        collectionSocialUnit = [...collectionSocialUnit, ...socialNetworkTypeSocialUnitList.socialUnitList];
      }
    });

    return collectionSocialUnit;
  }

  @Action(SocialUnitGetListAction)
  public getList(ctx: StateContext<SocialUnitModel>): Observable<ISocialNetworkTypeSocialUnitList[]> {
    return this._socialUnitService.getList().pipe(
      tap((socialNetworkTypeSocialUnitListArray) => {
        ctx.patchState({ socialNetworkTypeSocialUnitListArray });
      }),
    );
  }

  @Action(SocialUnitSwitchSocialNetworkTypeAction)
  public switchType(ctx: StateContext<SocialUnitModel>, action: SocialUnitSwitchSocialNetworkTypeAction): void {
    ctx.patchState({ currentSocialNetworkType: action.socialNetworkType });
  }

  @ImmutableContext()
  @Action(SocialUnitEnabledSwitchAction)
  public switchEnabled(ctx: StateContext<SocialUnitModel>, action: SocialUnitEnabledSwitchAction): Observable<ICollectionSocialUnit> {
    return this._socialUnitService.updateEnabledSwitch(action.socialUnitEnabledSwitch).pipe(
      tap((socialUnit) => {
        ctx.setState((state) => {
          for (const socialNetworkType of state.socialNetworkTypeSocialUnitListArray) {
            const socialUnitIndex: number = socialNetworkType.socialUnitList.findIndex(
              (_socialUnit) => _socialUnit._id === action.socialUnitEnabledSwitch.socialUnitId,
            );
            if (socialUnitIndex === -1) {
              continue;
            }
            socialNetworkType.socialUnitList[socialUnitIndex] = socialUnit;
            break;
          }
          return state;
        });
      }),
    );
  }
}
