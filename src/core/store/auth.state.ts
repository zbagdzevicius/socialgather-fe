import { Action, Selector, State, StateContext } from '@ngxs/store';
import { AuthService } from '../services/auth/auth.service';
import { tap } from 'rxjs/operators';
import { IUser, IUserLoginForm, IUserRegistrationForm } from '../models/user.interface';
import { IAuth, ILoginStatus } from '../models/auth.interface';
import { GoToCollectionSelectAction, GoToLoginAction } from './route.state';
import { Injectable } from '@angular/core';
import { SOCIAL_NETWORK_TYPE } from '../models/social-network.interface';
import { Observable } from 'rxjs';
import { IMessage } from '../models/app.interface';

export class LoginAction {
  public static readonly type: string = '[Auth] LoginAction';

  constructor(public userLoginForm: IUserLoginForm) {}
}

export class SocialLoginAction {
  public static readonly type: string = '[Auth] SocialLoginAction';

  constructor(public socialNetworkType: SOCIAL_NETWORK_TYPE) {}
}

export class RegistrationAction {
  public static readonly type: string = '[Auth] RegistrationAction';

  constructor(public userRegistrationForm: IUserRegistrationForm) {}
}

export class LogoutAction {
  public static readonly type: string = '[Auth] LogoutAction';
}

export class SocialLoginVerifyAction {
  public static readonly type: string = '[Auth] SocialLoginVerifyAction';

  constructor(public accessToken: string) {}
}

export class SocialNetworkAddAction {
  public static readonly type: string = '[SocialNetwork] SocialNetworkAddAction';

  constructor(public socialNetworkType: SOCIAL_NETWORK_TYPE) {}
}

const initialState: IAuth = {
  authenticated: false,
};

@Injectable()
@State<IAuth>({
  name: 'auth',
  defaults: initialState,
})
export class AuthState {
  @Selector()
  public static isAuthenticated(state: IAuth): boolean {
    return state.authenticated;
  }

  constructor(private authService: AuthService) {}

  @Action(LoginAction)
  public login(ctx: StateContext<IAuth>, action: LoginAction): Observable<ILoginStatus> {
    return this.authService.login(action.userLoginForm).pipe(
      tap((authorization: ILoginStatus) => {
        ctx.patchState({ authenticated: authorization.success });
        ctx.dispatch(new GoToCollectionSelectAction());
      }),
    );
  }

  @Action(SocialLoginAction)
  public socialLogin(ctx: StateContext<IAuth>, action: SocialLoginAction): void {
    return this.authService.socialLogin(action.socialNetworkType);
  }

  @Action(RegistrationAction)
  public registration(ctx: StateContext<IAuth>, action: RegistrationAction): Observable<IUser> {
    return this.authService.registration(action.userRegistrationForm).pipe(
      tap(() => {
        ctx.dispatch(new GoToLoginAction());
      }),
    );
  }

  @Action(SocialNetworkAddAction)
  public socialNetworkAdd(ctx: StateContext<IAuth>, action: SocialNetworkAddAction): void {
    return this.authService.addSocialNetwork(action.socialNetworkType);
  }

  @Action(SocialLoginVerifyAction)
  public socialNetworkLoginVerify(ctx: StateContext<IAuth>, action: SocialLoginVerifyAction): Observable<IMessage> {
    return this.authService.socialLoginVerify(action.accessToken).pipe(
      tap((verify) => {
        ctx.patchState({ authenticated: verify.success });
        if (verify.success) {
          ctx.dispatch(new GoToCollectionSelectAction());
        } else {
          ctx.dispatch(new GoToLoginAction());
        }
      }),
    );
  }

  @Action(LogoutAction)
  public logout(ctx: StateContext<IAuth>): Observable<void> {
    return this.authService.logout().pipe(tap(() => ctx.patchState({ authenticated: false })));
  }
}
