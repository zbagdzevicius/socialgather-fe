import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { IMenuItem } from '../models/app.interface';
import { SnackBarService } from '../services/snack-bar/snack-bar.service';
import { IModalOptions } from '../models/modal.interface';
import { ModalService } from '../services/modal/modal.service';

export class SnackBarOpenAction {
  public static readonly type: string = '[App] SnackBarOpenAction';

  constructor(public message: string) {}
}

export class ModalOpenAction {
  public static readonly type: string = '[App] ModalOpenAction';

  constructor(public modalOptions: IModalOptions) {}
}

export class LoadingStatusChangesAction {
  public static readonly type: string = '[App] LoadingStatusChangesAction';

  constructor(public isLoading: boolean) {}
}

export interface AppStateModel {
  lastSnackBarMessage: string;
  menu: IMenuItem[];
  isLoading: boolean;
  version: string;
}

const menu: IMenuItem[] = [
  {
    displayName: 'Statistics',
    link: '/statistics',
    icon: 'menuStatistics',
  },
  {
    displayName: 'Collections',
    link: '/collection',
    icon: 'menuCollections',
  },
  {
    displayName: 'Networks',
    link: '/collection/setup',
    icon: 'menuSetup',
  },
  {
    displayName: 'Publish',
    link: '/cron',
    icon: 'menuPublish',
  },
];

@Injectable()
@State<AppStateModel>({
  name: 'app',
  defaults: {
    lastSnackBarMessage: null,
    menu,
    isLoading: false,
    version: '0.0.2',
  },
})
export class AppState {
  constructor(private snackBarService: SnackBarService, private modalService: ModalService) {}

  @Selector()
  public static menu(state: AppStateModel): IMenuItem[] {
    return state.menu;
  }

  @Selector()
  public static isLoading(state: AppStateModel): boolean {
    return state.isLoading;
  }

  @Action(SnackBarOpenAction)
  public async openSnackBar(ctx: StateContext<AppStateModel>, action: SnackBarOpenAction): Promise<AppStateModel> {
    await this.snackBarService.presentToast(action.message);
    return ctx.patchState({ lastSnackBarMessage: action.message });
  }

  @Action(ModalOpenAction)
  public async openModal(ctx: StateContext<AppStateModel>, action: ModalOpenAction): Promise<void> {
    await this.modalService.openModal(action.modalOptions);
  }

  @Action(LoadingStatusChangesAction)
  public loadingStatusChanges(ctx: StateContext<AppStateModel>, action: LoadingStatusChangesAction): void {
    ctx.patchState({ isLoading: action.isLoading });
  }
}
