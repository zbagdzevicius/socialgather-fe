import { IStatistics, STATISTICS_CATEGORY } from '../models/statistics.interface';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { StatisticsService } from '../services/statistics/statistics.service';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

export class StatisticsGetAction {
  public static readonly type: string = '[Statistics] StatisticsGetAction';
}

export class StatisticsSwitchCategoryAction {
  public static readonly type: string = '[Statistics] StatisticsSwitchCategoryAction';

  constructor(public statisticsCategory: STATISTICS_CATEGORY) {}
}

export interface StatisticsStateModel extends IStatistics {
  type: 'daily' | 'weekly' | 'monthly';
  category: STATISTICS_CATEGORY;
}

@Injectable()
@State<StatisticsStateModel>({
  name: 'statistics',
  defaults: {
    pagesEngagedUsers: [],
    pagesFansLocationList: [],
    pagesImpressions: [],
    pagesReactions: [],
    pagesTotalActions: [],
    type: 'daily',
    category: STATISTICS_CATEGORY.ACTIONS,
  },
})
export class StatisticsState {
  constructor(private _statisticsService: StatisticsService) {}

  @Selector()
  public static statistics(state: StatisticsStateModel): IStatistics {
    return state;
  }

  @Selector()
  public static category(state: StatisticsStateModel): STATISTICS_CATEGORY {
    return state.category;
  }

  @Action(StatisticsGetAction)
  public getStatistics(ctx: StateContext<StatisticsStateModel>): Observable<IStatistics> {
    return this._statisticsService.get().pipe(
      tap((statistics) => {
        ctx.patchState({ ...statistics });
      }),
    );
  }

  @Action(StatisticsSwitchCategoryAction)
  public switchStatisticsCategory(ctx: StateContext<StatisticsStateModel>, action: StatisticsSwitchCategoryAction): void {
    ctx.patchState({ category: action.statisticsCategory });
  }
}
