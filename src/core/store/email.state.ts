import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { IMessage } from '../models/app.interface';
import { ICroniclerEmail } from '../models/cronicler-email.interface';
import { IEmailModalOptions } from '../models/modal.interface';
import { ApiServiceService } from '../services/api-service/api-service.service';

export class EmailSendAction {
  public static readonly type: string = '[Email] EmailSendAction';

  constructor(public emailForm: ICroniclerEmail) {}
}

export class OpenEmailModalAction {
  public static readonly type: string = '[Email] OpenEmailModalAction';

  constructor(public emailModalOptions: IEmailModalOptions) {}
}

export interface EmailStateModel {
  contacted: boolean;
}

@Injectable()
@State<EmailStateModel>({
  name: 'email',
  defaults: {
    contacted: false,
  },
})
export class EmailState {
  constructor(private _apiService: ApiServiceService) {}

  @Selector()
  public static contacted(state: EmailStateModel): boolean {
    return state.contacted;
  }

  @Action(EmailSendAction)
  public sendEmail(ctx: StateContext<EmailStateModel>, action: EmailSendAction): Observable<IMessage> {
    return this._apiService.sendEmail(action.emailForm).pipe(
      tap((emailResponse) => {
        if (emailResponse.success) {
          ctx.patchState({ contacted: true });
        }
      }),
    );
  }
}
