import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { ApiServiceService } from '../services/api-service/api-service.service';
import { Observable } from 'rxjs';
import { ICroniclerImage, ICroniclerImageSearch, IFileUploadResponse, IImageToUpload } from '../models/cronicler-image.interface';

export class ImageGetAction {
  public static readonly type: string = '[Image] ImageGetAction';

  constructor(public imageSearch: ICroniclerImageSearch) {}
}
export class ImageSelectAction {
  public static readonly type: string = '[Image] ImageSelectAction';

  constructor(public image: ICroniclerImage) {}
}
export class ImageUploadSelectAction {
  public static readonly type: string = '[Image] ImageUploadSelectAction';

  constructor(public image: IImageToUpload) {}
}
export class ImageUploadAction {
  public static readonly type: string = '[Image] ImageUploadAction';
}
export class ImageClearAction {
  public static readonly type: string = '[Image] ImageClearAction';
}
export class ImagesClearAction {
  public static readonly type: string = '[Image] ImagesClearAction';
}

export interface ImageStateModel {
  selectedImage: ICroniclerImage;
  selectedUploadImage: Blob;
  images: ICroniclerImage[];
  imageUploadResponse: IFileUploadResponse;
}

@Injectable()
@State<ImageStateModel>({
  name: 'image',
  defaults: {
    selectedImage: null,
    selectedUploadImage: null,
    images: null,
    imageUploadResponse: null,
  },
})
export class ImageState {
  constructor(private imageService: ApiServiceService) {}

  @Selector()
  public static selectedImage(state: ImageStateModel): ICroniclerImage {
    return state.selectedImage;
  }

  @Selector()
  public static images(state: ImageStateModel): ICroniclerImage[] {
    return state.images;
  }

  @Selector()
  public static imageUploadResponse(state: ImageStateModel): IFileUploadResponse {
    return state.imageUploadResponse;
  }

  @Action(ImageGetAction)
  public getImage(ctx: StateContext<ImageStateModel>, action: ImageGetAction): Observable<ICroniclerImage[]> {
    return this.imageService.getImage(action.imageSearch).pipe(
      tap((images) => {
        ctx.patchState({ images });
      }),
    );
  }

  @Action(ImageSelectAction)
  public selectImage(ctx: StateContext<ImageStateModel>, action: ImageSelectAction): void {
    ctx.patchState({ selectedImage: action.image, selectedUploadImage: null });
  }

  @Action(ImageUploadSelectAction)
  public uploadSelectImage(ctx: StateContext<ImageStateModel>, action: ImageUploadSelectAction): void {
    ctx.patchState({
      selectedImage: {
        fullImageUrl: action.image.imageUrl,
        thumbnailImageUrl: action.image.imageUrl,
        query: null,
      },
      selectedUploadImage: action.image.blob,
    });
  }

  @Action(ImageClearAction)
  public clearImage(ctx: StateContext<ImageStateModel>): void {
    ctx.patchState({ selectedImage: null, selectedUploadImage: null });
  }

  @Action(ImagesClearAction)
  public clearImages(ctx: StateContext<ImageStateModel>): void {
    ctx.patchState({ images: [] });
  }

  @Action(ImageUploadAction)
  public uploadImage(ctx: StateContext<ImageStateModel>): Observable<IFileUploadResponse> {
    return this.imageService.uploadImage(ctx.getState().selectedUploadImage).pipe(
      tap((imageUploadResponse) => {
        ctx.patchState({ imageUploadResponse, selectedUploadImage: null });
      }),
    );
  }
}
