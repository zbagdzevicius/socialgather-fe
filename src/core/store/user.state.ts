import { Action, Selector, State, StateContext } from '@ngxs/store';
import { IUser } from '../models/user.interface';
import { Injectable } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

export class GetSelfUserAction {
  public static readonly type: string = '[User] GetSelfUserAction';
}

export interface UserStateModel {
  user: IUser;
}

@Injectable()
@State<UserStateModel>({
  name: 'user',
  defaults: {
    user: null,
  },
})
export class UserState {
  constructor(private _userService: UserService) {}

  @Selector()
  public static user(state: UserStateModel): IUser | null {
    return state.user;
  }

  @Action(GetSelfUserAction)
  public getSelfUser(ctx: StateContext<UserStateModel>): Observable<IUser> {
    return this._userService.getSelfUser().pipe(
      tap((user) => {
        ctx.patchState({ user });
      }),
    );
  }
}
