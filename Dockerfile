# stage 1 install dependencies and
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install --only=development
RUN npm run build

# stage 2 make application available on localhost and make it available on localhost
EXPOSE 80
FROM nginx:alpine
COPY --from=node /app/dist/socialgather-fe /usr/share/nginx/html/
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
